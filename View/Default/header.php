<!doctype html>
<html lang="en">
<head>
    <title><?=$seo['title']?></title>
    <meta name="description" content="<?=$seo['desc']?>">
    <meta name="keywords" content="<?=$seo['key']?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="<?= $baseurl ?>/assets/images/content/fav_logo.png" type="image/png">
    <link rel="stylesheet" href="<?= $baseurl ?>/assets/javascript/lib/jquery.fancybox/source/jquery.fancybox.css">
    <link rel="stylesheet" href="<?= $baseurl ?>/assets/css/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?= $baseurl ?>/assets/css/bootstrap/css/bootstrap-theme.css">
    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="<?= $baseurl ?>/assets/css/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="<?= $baseurl ?>/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../assets/css/bootstrap/bootstrap-datetimepicker.css" />
    <link rel="stylesheet" href="../../assets/css/bootstrap/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="../../assets/css/bootstrap/bootstrap-datetimepicker-standalone.css" />
    <link rel="stylesheet" href="<?= $baseurl ?>/assets/css/swiper.css" media="all" />
    <script type="text/javascript"><?php echo "var base = '".$baseurl."';"; ?></script>
    <script src="<?= $baseurl ?>/assets/javascript/jquery2.2.4.min.js"></script>




    <script src="<?= $baseurl ?>/assets/css/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?= $baseurl ?>/assets/javascript/lib/jquery.fancybox/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
    <script src="<?=$baseurl?>/assets/javascript/bootstrap_datetimepicker.js"></script>
    <script src="<?= $baseurl ?>/assets/javascript/swiper.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= $baseurl ?>/assets/fullpage/fullpage.css" />
    <link rel="stylesheet" type="text/css" href="<?= $baseurl ?>/assets/fullpage/examples.css" />
    <script type="text/javascript" src="<?= $baseurl ?>/assets/fullpage/fullpage.js"></script>
    <script type="text/javascript" src="<?= $baseurl ?>/assets/fullpage/examples.js"></script>
	<link rel="stylesheet" type="text/css" href="<?= $baseurl ?>/assets/css/style.css">
    <script src="<?= $baseurl ?>/assets/javascript/script.js"></script>
    <script>
        $('.main_slider').slick({
            autoplay: false,
            arrows: false,
            dots: false,
            slidesToShow: 3,
            centerPadding: "10px",
            draggable: false,
            infinite: true,
            pauseOnHover: false,
            swipe: false,
            touchMove: false,
            vertical: true,
            speed: 1000,
            autoplaySpeed: 2000,
            useTransform: true,
            cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
            adaptiveHeight: true,
        });
    </script>
</head>
<body onload="mobStop()">
<?php  if(!isset($_COOKIE['video'])){ ?>
<div class="f_fid">
    <div class="f_vid_main">
        <video onended="myFunctionsss()" id="videoMain" width="500" height="500" autobuffer   autoplay="true" muted  playsinline>
            <source src="../../assets/videos/logo2_1.mp4" type='video/mp4;'>
            <source src="../../assets/videos/logo2_1.webm" type='video/webm;'>
        </video>
    </div>
</div>
<?php }  ?>
<header class="<?php
                if(isset($this->myPageMenu[0])){
                    if($this->myPageMenu[0] == ''){
                        echo 'head_main_stat';
                    }
                }
            ?>">
	<div class="head_main clear">
        <div class="col-lg-12 clear nopadd">
            <div class="col-lg-2 col-md-3 col-xs-5 nopadd clear has_inf">
                <div class="col-lg-4 col-md-3 col-xs-4 nopadd lang_butt">
                    <span class="<?= $this->lang == 'cz' ? 'lang_active' : '' ?> _langs" data-lang="cz">CZ</span>
                    <span class="<?= $this->lang == 'en' ? 'lang_active' : '' ?> _langs" data-lang="en">EN</span>
                </div>
                <div class="col-lg-8 col-md-9 col-xs-8 nopadd">
                    <div class="h_logo">
                        <a href="<?=$baseurl?>">
                            <img src="<?=$baseurl?>/assets/images/content/LogoHead.png" alt="Restaurant Avenue Logo">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-7 mob_none  nopadd clear inf_h_1005">
                <div class="h_menu_main">
                    <ul>
                        <li>
                            <?php  if($_SERVER['REQUEST_URI'] != '/'){
                                echo '<a href="'.$baseurl.'"/>';
                            }  ?>
                            <span class=" rest_but
                                <?php
                                    if(isset($this->myPageMenu[0])){
                                        if($this->myPageMenu[0] == ''){
                                            echo 'h_menu_active';
                                        }
                                    }
                                ?>
                            "><?= $this->lang == 'en' ? 'RESTAURANT' : 'RESTAURACE' ?></span>
                            <?php  if($_SERVER['REQUEST_URI'] != '/'){
                                echo '</a>';
                            }  ?>
                        </li>
                        <li>
                            <a href="<?=$baseurl?>/menu/">
                                <span class="
                                    <?php
                                        if(isset($this->myPageMenu[0])){
                                            if($this->myPageMenu[0] == 'menu'){
                                                echo 'h_menu_active';
                                            }
                                        }
                                    ?>
                                "><?= $this->lang == 'en' ? 'MENU' : 'MENU' ?></span>
                            </a>
                        </li>
                        <li>
                            <span class="events_but
                                <?php
                                    if(isset($this->myPageMenu[0])){
                                        if($this->myPageMenu[0] == 'events'){
                                            echo 'h_menu_active';
                                        }
                                    }
                                ?>
                            "><?= $this->lang == 'en' ? 'EVENTS' : 'AKCE ' ?></span>
                        </li>
                        <li>
                            <span class="gallery_but
                            <?php
                                if(isset($this->myPageMenu[0])){
                                    if($this->myPageMenu[0] == 'gallery'){
                                        echo 'h_menu_active';
                                    }
                                }
                            ?>
                            "><?= $this->lang == 'en' ? 'GALLERY' : 'GALERIE' ?></span>
                        </li>
                        <li>
                            <a href="<?=$baseurl?>/contacts/">
                                <span class="
                                    <?php
                                    if(isset($this->myPageMenu[0])){
                                        if($this->myPageMenu[0] == 'contacts'){
                                            echo 'h_menu_active';
                                        }
                                    }
                                    ?>
                                "><?= $this->lang == 'en' ? 'CONTACTS' : 'KONTAKTY' ?></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>






            <div class="navigation_mob">
                <span class="btnMobMenu mobclose mob_button"><i class="fa fa-times " aria-hidden="true"></i></span>
                <div class="col-lg-8 col-md-7  nopadd clear">
                    <div class="h_menu_main">
                        <ul>
                            <li>
                                <?php  if($_SERVER['REQUEST_URI'] != '/'){
                                    echo '<a href="'.$baseurl.'"/>';
                                }  ?>
                                <span class=" rest_but
                                    <?php
                                if(isset($this->myPageMenu[0])){
                                    if($this->myPageMenu[0] == ''){
                                        echo 'h_menu_active';
                                    }
                                }
                                ?>
                                "><?= $this->lang == 'en' ? 'RESTAURANT' : 'RESTAURACE' ?></span>
                                <?php  if($_SERVER['REQUEST_URI'] != '/'){
                                    echo '</a>';
                                }  ?>
                            </li>
                            <li>
                                <a href="<?=$baseurl?>/menu/">
                                    <span class="
                                        <?php
                                    if(isset($this->myPageMenu[0])){
                                        if($this->myPageMenu[0] == 'menu'){
                                            echo 'h_menu_active';
                                        }
                                    }
                                    ?>
                                    "><?= $this->lang == 'en' ? 'MENU' : 'MENU' ?></span>
                                </a>
                            </li>
                            <li>
                                <span class="events_but
                                    <?php
                                if(isset($this->myPageMenu[0])){
                                    if($this->myPageMenu[0] == 'events'){
                                        echo 'h_menu_active';
                                    }
                                }
                                ?>
                                "><?= $this->lang == 'en' ? 'EVENTS' : 'AKCE' ?></span>
                            </li>
                            <li>
                                <span class="gallery_but
                                <?php
                                if(isset($this->myPageMenu[0])){
                                    if($this->myPageMenu[0] == 'gallery'){
                                        echo 'h_menu_active';
                                    }
                                }
                                ?>
                                "><?= $this->lang == 'en' ? 'GALLERY' : 'GALERIE' ?></span>
                            </li>
                            <li>
                                <a href="<?=$baseurl?>/contacts/">
                                    <span class="
                                        <?php
                                    if(isset($this->myPageMenu[0])){
                                        if($this->myPageMenu[0] == 'contacts'){
                                            echo 'h_menu_active';
                                        }
                                    }
                                    ?>
                                    "><?= $this->lang == 'en' ? 'CONTACTS' : 'KONTAKTY' ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>



                <div class="restaurant_sub_head">
                    <div class="content">
                                        <ul>
                                            <li class="restaurant_sub_head_active rest_menus" data-ind="0"><?= $this->lang == 'en' ? 'ABOUT RESTAURANT' : 'O NÁS' ?></li>
                                            <li class="rest_menus" data-ind="1">CHEF</li>
                                            <!--                <li>PRESS ABOUT US</li>-->
                                            <li class="rest_menus" data-ind="2"><?= $this->lang == 'en' ? 'CLUB CARD' : 'KLUBOVÁ KARTA' ?></li>
                                            <li class="rest_menus" data-ind="3"><?= $this->lang == 'en' ? '3D TOUR' : '3D TOUR' ?></li>
                                            <li class="rest_menus" data-ind="4"><?= $this->lang == 'en' ? 'REVIEW' : 'HODNOCENÍ' ?></li>
                                        </ul>
                    </div>
                </div>
                <div class="events_sub_head">
                    <div class="content">
                        <ul>
                            <li class="
                     <?php
                            if(isset($this->myPageMenu[1])){
                                if($this->myPageMenu[1] == 'upcoming'){
                                    echo 'events_sub_head_active';
                                }
                            }
                            ?>
                ">
                                <a href="<?=$baseurl?>/events/upcoming/">
                                    <?= $this->lang == 'en' ? 'UPCOMING' : 'PŘEDCHOZÍ' ?>
                                </a>
                            </li>
                            <li class="
                    <?php
                            if(isset($this->myPageMenu[1])){
                                if($this->myPageMenu[1] == 'archive'){
                                    echo 'events_sub_head_active';
                                }
                            }
                            ?>
                ">
                                <a href="<?=$baseurl?>/events/archive/">
                                    <?= $this->lang == 'en' ? 'ARCHIVE' : 'ARCHIV' ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="gallery_sub_head">
                    <div class="content">
                        <ul>
                            <li class="
                    <?php
                            if(isset($this->myPageMenu[1])){
                                if($this->myPageMenu[1] == 'interior'){
                                    echo 'gallery_sub_head_active';
                                }
                            }
                            ?>
                ">
                                <a href="<?=$baseurl?>/gallery/interior/">
                                    <?= $this->lang == 'en' ? 'INTERIOR' : 'INTERIÉR' ?>
                                </a>
                            </li>
                            <li class="
                    <?php
                            if(isset($this->myPageMenu[1])){
                                if($this->myPageMenu[1] == 'food'){
                                    echo 'gallery_sub_head_active';
                                }
                            }
                            ?>
                ">
                                <a href="<?=$baseurl?>/gallery/food/">
                                    <?= $this->lang == 'en' ? 'FOOD' : 'JÍDLA' ?>
                                </a>
                            </li>
                            <li class="
                    <?php
                            if(isset($this->myPageMenu[1])){
                                if($this->myPageMenu[1] == 'events'){
                                    echo 'gallery_sub_head_active';
                                }
                            }
                            ?>
                ">
                                <a href="<?=$baseurl?>/gallery/events/">
                                    <?= $this->lang == 'en' ? 'EVENTS' : 'AKCE' ?>
                                </a>
                            </li>
                            <li class="
                    <?php
                            if(isset($this->myPageMenu[1])){
                                if($this->myPageMenu[1] == 'video'){
                                    echo 'gallery_sub_head_active';
                                }
                            }
                            ?>
                ">
                                <a href="<?=$baseurl?>/gallery/video/">
                                    VIDEO
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div  class="mob_menu_socials">
                    <p class="c_l_social">
                        <a href="https://facebook.com/avenueprague" target="_blank"><span><i class="fa fa-facebook-f"></i></span></a>
                        <a href="https://instagram.com/avenue__restobar" target="_blank"><span><i class="fa fa-instagram"></i></span></a>
<!--                        <a href="#"><span><img src="--><?//=$baseurl?><!--/assets/images/content/main_land/telegram.png"></span></a>-->
                        <a href="https://cz.pinterest.com/avenuerestobarlounge/" target="_blank"><span><i class="fa fa-pinterest-p"></i></span></a>
                        <a href="https://www.tripadvisor.cz/Restaurant_Review-g274707-d14142527-Reviews-Avenue_restaurant_bar-Prague_Bohemia.html" target="_blank"><span><i class="fa fa-tripadvisor"></i></span></a>
                    </p>
                </div>
            </div>

            <div class="col-lg-2 col-md-2 col-xs-5 nopadd clear res_butt_infs">
                <div class="h_reserve_button">
                    <a href="<?=$baseurl?>/reservation/">
                        <span class="h_reserve_button_link
                                    <?php
                                        if($_SERVER['REQUEST_URI'] == '/reservation/'){
                                            echo 'h_reserve_button_link_active';
                                        }
                                    ?>">
                            <?= $this->lang == 'en' ? 'RESERVATION' : 'REZERVACE' ?>
                        </span>
                    </a>
                </div>
            </div>
            <div class="mob_button mob_butt_st  col-md-2 col-xs-2 nopadd">
                <span>
                    <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
                </span>
            </div>

        </div>
    </div>


    <div class="main_sub_heads">
        <div class="restaurant_sub_head">
            <div class="content">
                <ul id="menu">
                    <li data-menuanchor="firstPage" class="  active" ><a href="#firstPage"><?= $this->lang == 'en' ? 'ABOUT RESTAURANT' : 'O NÁS' ?></a></li>
                    <li data-menuanchor="secondPage" class="" ><a href="#secondPage">CHEF</a></li>
                    <!--                <li>PRESS ABOUT US</li>-->
                    <li data-menuanchor="3rdPage" class="" ><a href="#3rdPage"><?= $this->lang == 'en' ? 'CLUB CARD' : 'KLUBOVÁ KARTA' ?></a></li>
                    <li data-menuanchor="4rdPage" class="" ><a href="#4rdPage"><?= $this->lang == 'en' ? '3D TOUR' : '3D TOUR' ?></a></li>
                    <li data-menuanchor="5rdPage" class="" ><a href="#5rdPage"><?= $this->lang == 'en' ? 'REVIEW' : 'HODNOCENÍ' ?></a></li>
                </ul>
            </div>
        </div>
        <div class="events_sub_head">
            <div class="content">
                <ul>
                    <li class="
                     <?php
                    if(isset($this->myPageMenu[1])){
                        if($this->myPageMenu[1] == 'upcoming'){
                            echo 'events_sub_head_active';
                        }
                    }
                    ?>
                ">
                        <a href="<?=$baseurl?>/events/upcoming/">
                            <?= $this->lang == 'en' ? 'UPCOMING' : 'PŘEDCHOZÍ' ?>
                        </a>
                    </li>
                    <li class="
                    <?php
                    if(isset($this->myPageMenu[1])){
                        if($this->myPageMenu[1] == 'archive'){
                            echo 'events_sub_head_active';
                        }
                    }
                    ?>
                ">
                        <a href="<?=$baseurl?>/events/archive/">
                            <?= $this->lang == 'en' ? 'ARCHIVE' : 'ARCHIV' ?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="gallery_sub_head">
            <div class="content">
                <ul>
                    <li class="
                    <?php
                    if(isset($this->myPageMenu[1])){
                        if($this->myPageMenu[1] == 'interior'){
                            echo 'gallery_sub_head_active';
                        }
                    }
                    ?>
                ">
                        <a href="<?=$baseurl?>/gallery/interior/">
                            <?= $this->lang == 'en' ? 'INTERIOR' : 'INTERIÉR' ?>
                        </a>
                    </li>
                    <li class="
                    <?php
                    if(isset($this->myPageMenu[1])){
                        if($this->myPageMenu[1] == 'food'){
                            echo 'gallery_sub_head_active';
                        }
                    }
                    ?>
                ">
                        <a href="<?=$baseurl?>/gallery/food/">
                            <?= $this->lang == 'en' ? 'FOOD' : 'JÍDLA' ?>
                        </a>
                    </li>
                    <li class="
                    <?php
                    if(isset($this->myPageMenu[1])){
                        if($this->myPageMenu[1] == 'events'){
                            echo 'gallery_sub_head_active';
                        }
                    }
                    ?>
                ">
                        <a href="<?=$baseurl?>/gallery/events/">
                            <?= $this->lang == 'en' ? 'EVENTS' : 'AKCE' ?>
                        </a>
                    </li>
                    <li class="
                    <?php
                    if(isset($this->myPageMenu[1])){
                        if($this->myPageMenu[1] == 'video'){
                            echo 'gallery_sub_head_active';
                        }
                    }
                    ?>
                ">
                        <a href="<?=$baseurl?>/gallery/video/">
                            VIDEO
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

</header>