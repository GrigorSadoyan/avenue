<link rel="stylesheet" type="text/css" href="<?=$baseurl?>/assets/css/main_land.css" />
<audio  hidden id="player_audio" >
    <source src="<?=$baseurl?>/assets/music/<?=$params['music'][0]['music']?>" type="audio/mpeg">
</audio>
<script>
    function mobStop() {
        var audWidth = $(window).width();
        var x = document.getElementById("player_audio");
        if( audWidth <= 425 ){
            x.pause();
        }else{
            x.play();
        }
    }
</script>
<div id="" class="main_slider main_mob_back">
    <div class="m_socials">
        <ul class="soc_m_ul">
            <li class="vol_min">
                <span class="vol_min_icon">
                    <i class="fa fa-volume-off"></i>
                </span>
            </li>
            <li class="vol_pl">
                <span class="vol_plus_icon">
                    <i class="fa fa-volume-up"></i>
                </span>
            </li>
            <li>
                <a href="https://facebook.com/avenueprague" target="_blank">
                    <span class="face_icon">
                        <i class="fa fa-facebook-f"></i>
                    </span>
                </a>
            </li>
            <li>
                <a href="https://www.instagram.com/avenue__prague/" target="_blank">
                    <span class="insta_icon">
                        <i class="fa fa-instagram"></i>
                    </span>
                </a>
            </li>
<!--            <li>-->
<!--                <span class="tele_icon">-->
<!--                    <img src="--><?//=$baseurl?><!--/assets/images/content/main_land/telegram.png">-->
<!--                </span>-->
<!--            </li>-->
            <li>
                <a href="https://cz.pinterest.com/avenuerestaurant/pins/" target="_blank">
                    <span class="pint_icon">
                        <i class="fa fa-pinterest-p"></i>
                    </span>
                </a>
            </li>
            <li>
                <a href="https://www.tripadvisor.cz/Restaurant_Review-g274707-d14142527-Reviews-Avenue_restaurant_bar-Prague_Bohemia.html" target="_blank">
                    <span class="trip_icon">
                        <i class="fa fa-tripadvisor"></i>
                    </span>
                </a>
            </li>

        </ul>
    </div>
    <div class="main_each_mob">
        <div id="single">
            <div data-target="home" id="home">
                <div class="content-resizer">
                    <div class="main_cont">
                        <div class="section_back_img">
                            <img src="<?=$baseurl?>/assets/images/content/main_land/1.jpg">
                        </div>
                        <div class="mains_conts">
                            <div class="land_cont">
                                <div class="about_all">
                                    <p class="about_title"><?= $this->lang == 'en' ? 'ABOUT RESTAURANT' : 'O RESTAURACI' ?></p>
                                    <div class="about_text">
										<?=$params['about_rest']['text_'.$this->lang]?>
									</div>
                                </div>
                            </div>
                        </div>
                        <div class="mi_black"></div>
                        <div class="arrow_nexts" data-index="1">
                            <img src="<?=$baseurl?>/assets/images/content/main_land/arrow_next.png">
                        </div>
                    </div>
                </div>
            </div>

            <div data-target="about" id="about">
                <div class="content-resizer">
                    <div class="main_cont">
                        <div class="section_back_img">
                            <img src="<?=$baseurl?>/assets/images/content/main_land/2.jpg">
                        </div>
                        <div class="mains_conts">
                            <div class="land_cont">
                                <div class="chef_all">
                                    <p class="chef_title">CHEF</p>
                                    <div class="chef_img bor">
                                        <img src="<?=$baseurl?>/assets/images/content/main_land/chef.png">
                                    </div>
                                    <p class="chef_name">
                                        Victor Beley
                                    </p>
                                    <p class="chef_cit">
                                        ”Chef of the restaurant Avenue”
                                    </p>
                                     <div class="chef_text">
                                        <?=$params['chef_text']['text_'.$this->lang]?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mi_black"></div>
                        <div class="arrow_nexts" data-index="2">
                            <img src="<?=$baseurl?>/assets/images/content/main_land/arrow_next.png">
                        </div>
                    </div>
                </div>
            </div>

            <div data-target="menu" id="menu">
                <div class="content-resizer">
                    <div class="main_cont">
                        <div class="section_back_img">
                            <!--                    <img src="--><?//=$baseurl?><!--/assets/images/content/main_land/2.jpg">-->
                        </div>
                        <div class="mains_conts clear" >
                            <div class="card_pic  col-lg-10 col-md-10 col-sm-10 col-xs-12  center clear">
<!--                                <img src="--><?//=$baseurl?><!--/assets/images/content/cardlogo.png">-->
                                <div class=" card_image col-lg-6 col-md-6 col-sm-6  col-xs-6 nopadd">
                                    <img src="<?=$baseurl?>/assets/images/avenue-discount-card 1-01.png" alt="avenue-discount-card">
                                </div>
                                <div class="card_image col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadd">
                                    <img src="<?=$baseurl?>/assets/images/avenue-discount-card 1_-01.png" alt="avenue-discount-card">
                                </div>

                            </div>
                            <?php if($this->lang == 'en'){ ?>
                                <div class="land_clubCont">
                                    <div class="chef_all">
                                        <p class="club_title">CLUB CARD</p>
                                        <p class="clubCard_terms">TERMS OF USE OF REGULAR GUEST CARD</p>
<!--                                        <p class="club_term_row"></p>-->
                                        <div class="card_land_cont air_cont">
                                            <p class="clubCard_rows row_out_circle">
                                                Are you a lover of delicious dishes and unique cocktails? Pick up your Avenue Club Card completely for FREE at our restaurant!
                                            </p>
                                            <p class="clubCard_rows row_out_circle">
                                                What are the benefits with club card and terms of use?
                                            </p>
                                            <p class="clubCard_rows">
                                                <span></span>
                                                Discount 10% and other special discounts
                                            </p>
                                            <p class="clubCard_rows">
                                                <span></span>
                                                Special events for club card holders
                                            </p>
                                            <p class="clubCard_rows">
                                                <span></span>
                                                Notifications via SMS or a newsletter about the ongoing action
                                            </p>
                                            <p class="clubCard_rows">
                                                <span></span>
                                                The club card holder may be a person over 18 years of age and can not be used by another person
                                            </p>
                                            <p class="clubCard_rows">
                                                <span></span>
                                                Agree to General Terms and Conditions for Cardholders
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php  }else{ ?>
                                <div class="land_clubCont">
                                    <div class="chef_all">
                                        <p class="club_title">CLUB CARD</p>
                                        <p class="clubCard_terms">TERMS OF USE OF REGULAR GUEST CARD</p>
<!--                                        <p class="club_term_row"></p>-->
                                        <div class="card_land_cont air_cont">
                                            <p class="clubCard_rows row_out_circle">
                                                Jste milovníkem vynikajících pokrmů a unikátních koktejlů? Vyzvedněte si v naši restauraci Avenue klubovou kartu zcela ZDARMA!
                                            </p>
                                            <p class="clubCard_rows row_out_circle">
                                                Jaké výhody s klubovou kartou získáte a podmínky využití klubové karty?
                                            </p>
                                            <p class="clubCard_rows">
                                                <span></span>
                                                Sleva 10% a veškeré menu v naši restauraci
                                            </p>
                                            <p class="clubCard_rows">
                                                <span></span>
                                                Speciální akce pro držitele klubové karty
                                            </p>
                                            <p class="clubCard_rows">
                                                <span></span>
                                                Informování prostřednictvím newsletteru nebo SMS o probíhající akci
                                            </p>
                                            <p class="clubCard_rows">
                                                <span></span>
                                                Držitelem klubové karty smí být osoba starší 18 let a nemůže být využívána jinou osobou
                                            </p>
                                            <p class="clubCard_rows">
                                                <span></span>
                                                Souhlas se všeobecnými podmínkami pro držitele karty
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php  } ?>
                        </div>
                        <div class="mi_black"></div>
                        <div class="arrow_nexts" data-index="3">
                            <img src="<?=$baseurl?>/assets/images/content/main_land/arrow_next.png">
                        </div>
                    </div>
                </div>
            </div>

            <div data-target="examples" id="examples">
                <div class="content-resizer">
                    <div class="main_cont">
                        <div class="section_back_img">
                            <!--                    <img src="--><?//=$baseurl?><!--/assets/images/content/main_land/1.jpg">-->
                        </div>
                        <div class="mains_conts">
                            <div class="d_main">
                                <div class="d_sections">
                                    <p class="d_title">3D TOUR</p>
                                </div>
                                <div class="d_sections">
                                    <div class="d_img">
                                        <img src="<?=$baseurl?>/assets/images/content/main_land/d1.jpg">
                                    </div>
                                    <div class="d_title_secont">
                                        <p class="d_title_s">
                                            DAY
                                        </p>
                                        <p class="club_term_row"></p>
                                    </div>
                                    <div class="d_mi_black"></div>
                                </div>
                                <div class="d_sections">
                                    <div class="d_img">
                                        <img src="<?=$baseurl?>/assets/images/content/main_land/d2.jpg">
                                    </div>
                                    <div class="d_title_secont">
                                        <p class="d_title_s">
                                            NIGHT
                                        </p>
                                        <p class="club_term_row"></p>
                                    </div>
                                    <div class="d_mi_black"></div>
                                </div>
                                <div class="d_sections">
                                    <div class="d_img">
                                        <img src="<?=$baseurl?>/assets/images/content/main_land/d3.jpg">
                                    </div>
                                    <div class="d_title_secont">
                                        <p class="d_title_s">
                                            PRAHA PANORAMA
                                        </p>
                                        <p class="club_term_row"></p>
                                    </div>
                                    <div class="d_mi_black"></div>
                                </div>
                            </div>
                        </div>
                        <div class="mi_black"></div>
                        <div class="arrow_nexts"  data-index="4">
                            <img src="<?=$baseurl?>/assets/images/content/main_land/arrow_next.png">
                        </div>
                    </div>
                </div>
            </div>

            <div data-target="contact" id="contact">
                <div class="content-resizer">
                    <div class="main_cont">
                        <div class="section_back_img">
                            <img src="<?=$baseurl?>/assets/images/content/main_land/4.jpg">
                        </div>
                        <div class="mains_conts">
                            <div class="rev_cont">
                                <p class="feed_title"><?= $this->lang == 'en' ? 'REVIEW' : 'HODNOCENÍ '?></p>
                                <p class="res_title"><?= $this->lang == 'en' ? 'SEND A MESSAGE' : 'ODESLAT  ZPRÁVU'?></p>
                                <!--                        <form action="--><?//=$baseurl?><!--/message/review/" method="post">-->
                                <p class="res_row_input">
                                    <input class="rev_name" type="text" placeholder="<?= $this->lang == 'en' ? 'Name' : 'Jméno'?>*">
                                </p>
                                <p class="res_row_input">
                                    <input class="rev_phone" type="text" name="phone" placeholder="<?= $this->lang == 'en' ? 'Phone' : 'Telefonní' ?>*">
                                </p>
                                <p class="res_row_input">
                                    <input class="rev_email" type="email" name="email" placeholder="<?= $this->lang == 'en' ? 'E-Mail' : 'Emailová' ?>*">
                                </p>
                                <p class="res_row_input">
                                    <textarea class="rev_text" rows="6" name="review" placeholder="<?= $this->lang == 'en' ? 'Review' : 'HODNOCENÍ '?>*"></textarea>
                                </p>
                                <p class="res_row_button">
                                    <span><input class="rev_send __send_rev" type="submit" value="<?= $this->lang == 'en' ? 'SEND' : 'ODESLAT '?>"></span>
                                </p>
                                <!--                        </form>-->
                            </div>
                        </div>
                        <div class="mi_black"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="main_each_pc">
        <div id="fullpage">

            <div class="section " id="section0">
                <!--        <div class="content-resizer">-->
                <div class="main_cont">
                    <div class="section_back_img">
                        <img src="<?=$baseurl?>/assets/images/content/main_land/1.jpg">
                    </div>
                    <div class="mains_conts">
                        <div class="land_cont">
                            <div class="about_all">
                                <p class="about_title"><?= $this->lang == 'en' ? 'ABOUT RESTAURANT' : 'O RESTAURACI' ?></p>
                                <div class="about_text">
                                    <?=$params['about_rest']['text_'.$this->lang]?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mi_black"></div>
                    <!--                            <div class="arrow_nexts" data-index="1">-->
                    <!--                                <img src="--><?//=$baseurl?><!--/assets/images/content/main_land/arrow_next.png">-->
                    <!--                            </div>-->
                </div>
                <!--                    </div>-->

            </div>

            <div class="section" id="section1">
                <div class="content-resizer">
                    <div class="main_cont">
                        <div class="section_back_img">
                            <img src="<?=$baseurl?>/assets/images/content/main_land/2.jpg">
                        </div>
                        <div class="mains_conts">
                            <div class="land_cont">
                                <div class="chef_all">
                                    <p class="chef_title">CHEF</p>
                                    <div class="chef_img bor">
                                        <img class="imgefect" src="<?=$baseurl?>/assets/images/content/main_land/chef.png">
                                    </div>
                                    <p class="chef_name">
                                        Victor Beley
                                    </p>
                                    <p class="chef_cit">
                                        ”Chef of the restaurant Avenue”
                                    </p>
                                    <div class="chef_text">
                                        <?=$params['chef_text']['text_'.$this->lang]?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mi_black"></div>
                        <!--                        <div class="arrow_nexts" data-index="2">-->
                        <!--                            <img src="--><?//=$baseurl?><!--/assets/images/content/main_land/arrow_next.png">-->
                        <!--                        </div>-->
                    </div>
                </div>
            </div>

            <div class="section" id="section2">
                <div class="content-resizer">
                    <div class="main_cont">
                        <div class="section_back_img">
<!--                            <img src="--><?//=$baseurl?><!--/assets/images/content/cardlogo.png">-->
                        </div>
                        <div class="mains_conts clear">
<!--                                <img src="--><?//=$baseurl?><!--/assets/images/content/cardlogo.png">-->
                            <div class="card_pic  col-lg-10 col-md-10 col-sm-10 col-xs-12 center clear">
                                <!--                                <img src="--><?//=$baseurl?><!--/assets/images/content/cardlogo.png">-->
                                <div class=" card_image col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadd">
                                    <img src="<?=$baseurl?>/assets/images/avenue-discount-card 1-01.png" alt="avenue-discount-card">
                                </div>
                                <div class="card_image col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadd">
                                    <img src="<?=$baseurl?>/assets/images/avenue-discount-card 1_-01.png" alt="avenue-discount-card">
                                </div>

                            </div>
                            <?php if($this->lang == 'en'){ ?>
                            <div class="land_clubCont">
                                <div class="chef_all">
                                    <p class="club_title">CLUB CARD</p>
                                    <p class="clubCard_terms">TERMS OF USE OF REGULAR GUEST CARD</p>

                                    <div class="card_land_cont air_cont">
                                        <p class="clubCard_rows row_out_circle">
                                            Are you a lover of delicious dishes and unique cocktails? Pick up your Avenue Club Card completely for FREE at our restaurant!
                                        </p>
                                        <p class="clubCard_rows row_out_circle">
                                            What are the benefits with club card and terms of use?
                                        </p>
                                        <p class="clubCard_rows">
                                            <span></span>
                                            Discount 10% and other special discounts
                                        </p>
                                        <p class="clubCard_rows">
                                            <span></span>
                                            Special events for club card holders
                                        </p>
                                        <p class="clubCard_rows">
                                            <span></span>
                                            Notifications via SMS or a newsletter about the ongoing action
                                        </p>
                                        <p class="clubCard_rows">
                                            <span></span>
                                            The club card holder may be a person over 18 years of age and can not be used by another person
                                        </p>
                                        <p class="clubCard_rows">
                                            <span></span>
                                            Agree to General Terms and Conditions for Cardholders
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php  }else{ ?>
                                <div class="land_clubCont">
                                    <div class="chef_all">
                                        <p class="club_title">CLUB CARD</p>
                                        <p class="clubCard_terms">TERMS OF USE OF REGULAR GUEST CARD</p>

                                        <div class="card_land_cont air_cont">
                                            <p class="clubCard_rows row_out_circle">
                                                Jste milovníkem vynikajících pokrmů a unikátních koktejlů? Vyzvedněte si v naši restauraci Avenue klubovou kartu zcela ZDARMA!
                                            </p>
                                            <p class="clubCard_rows row_out_circle">
                                                Jaké výhody s klubovou kartou získáte a podmínky využití klubové karty?
                                            </p>
                                            <p class="clubCard_rows">
                                                <span></span>
                                                Sleva 10% a veškeré menu v naši restauraci
                                            </p>
                                            <p class="clubCard_rows">
                                                <span></span>
                                                Speciální akce pro držitele klubové karty
                                            </p>
                                            <p class="clubCard_rows">
                                                <span></span>
                                                Informování prostřednictvím newsletteru nebo SMS o probíhající akci
                                            </p>
                                            <p class="clubCard_rows">
                                                <span></span>
                                                Držitelem klubové karty smí být osoba starší 18 let a nemůže být využívána jinou osobou
                                            </p>
                                            <p class="clubCard_rows">
                                                <span></span>
                                                Souhlas se všeobecnými podmínkami pro držitele karty
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php  } ?>
                        </div>
                        <div class="mi_black"></div>
                        <!--                <div class="arrow_nexts" data-index="3">-->
                        <!--                    <img src="--><?//=$baseurl?><!--/assets/images/content/main_land/arrow_next.png">-->
                        <!--                </div>-->
                    </div>
                </div>
            </div>
            <div class="section" id="section3">
                <div class="content-resizer">
                    <div class="main_cont">
                        <div class="section_back_img">
                            <!--                    <img src="--><?//=$baseurl?><!--/assets/images/content/main_land/1.jpg">-->
                        </div>
                        <div class="mains_conts">
                            <div class="d_main">
                                <div class="d_sections">
                                    <p class="d_title">3D TOUR</p>
                                </div>
                                <div class="d_sections">
                                    <div class="d_img">
                                        <img src="<?=$baseurl?>/assets/images/content/main_land/d1.jpg">
                                    </div>
                                    <div class="d_title_secont">
                                        <p class="d_title_s">
                                            DAY
                                        </p>
                                        <p class="club_term_row"></p>
                                    </div>
                                    <div class="d_mi_black"></div>
                                </div>
                                <div class="d_sections">
                                    <div class="d_img">
                                        <img src="<?=$baseurl?>/assets/images/content/main_land/d2.jpg">
                                    </div>
                                    <div class="d_title_secont">
                                        <p class="d_title_s">
                                            NIGHT
                                        </p>
                                        <p class="club_term_row"></p>
                                    </div>
                                    <div class="d_mi_black"></div>
                                </div>
                                <div class="d_sections">
                                    <div class="d_img">
                                        <img src="<?=$baseurl?>/assets/images/content/main_land/d3.jpg">
                                    </div>
                                    <div class="d_title_secont">
                                        <p class="d_title_s">
                                            PRAHA PANORAMA
                                        </p>
                                        <p class="club_term_row"></p>
                                    </div>
                                    <div class="d_mi_black"></div>
                                </div>
                            </div>
                        </div>
                        <div class="mi_black"></div>
                        <!--                        <div class="arrow_nexts"  data-index="4">-->
                        <!--                            <img src="--><?//=$baseurl?><!--/assets/images/content/main_land/arrow_next.png">-->
                        <!--                        </div>-->
                    </div>
                </div>

            </div>
            <div class="section" id="section4">
                <div class="content-resizer">
                    <div class="main_cont">
                        <div class="section_back_img">
                            <img src="<?=$baseurl?>/assets/images/content/main_land/4.jpg">
                        </div>
                        <div class="mains_conts">
                            <div class="rev_cont">
                                <p class="feed_title"><?= $this->lang == 'en' ? 'REVIEW' : 'HODNOCENÍ '?></p>
                                <p class="res_title"><?= $this->lang == 'en' ? 'SEND A MESSAGE' : 'ODESLAT  ZPRÁVU'?></p>
                                <!--                        <form action="--><?//=$baseurl?><!--/message/review/" method="post">-->
                                <p class="res_row_input">
                                    <input class="rev_name" type="text" placeholder="<?= $this->lang == 'en' ? 'Name' : 'Jméno'?>*">
                                </p>
                                <p class="res_row_input">
                                    <input class="rev_phone" type="text" name="phone" placeholder="<?= $this->lang == 'en' ? 'Phone' : 'Telefonní' ?>*">
                                </p>
                                <p class="res_row_input">
                                    <input class="rev_email" type="email" name="email" placeholder="<?= $this->lang == 'en' ? 'E-Mail' : 'Emailová' ?>*">
                                </p>
                                <p class="res_row_input">
                                    <textarea class="rev_text" rows="6" name="review" placeholder="<?= $this->lang == 'en' ? 'Review' : 'HODNOCENÍ '?>*"></textarea>
                                </p>
                                <p class="res_row_button">
                                    <span><input class="rev_send __send_rev" type="submit" value="<?= $this->lang == 'en' ? 'SEND' : 'ODESLAT '?>"></span>
                                </p>
                                <!--                        </form>-->
                            </div>
                        </div>
                        <div class="mi_black"></div>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <!-- vertical nav -->
    <div id="fp-nav" class="right" style="margin-top: -43.5px;">
        <ul class="g_lis">
            <li><a href="#" class="active" data-anchor="home" data-in="0"><span></span></a>
                <div class="fp-tooltip right">Home</div>
            </li>
            <li><a href="#"  data-anchor="about" data-in="1"><span></span></a>
                <div class="fp-tooltip right">About</div>
            </li>
            <li><a href="#"  data-anchor="menu" data-in="2"><span></span></a>
                <div class="fp-tooltip right">menu</div>
            </li>
            <li><a href="#"  data-anchor="examples" data-in="3"><span></span></a>
                <div class="fp-tooltip right">Examples</div>
            </li>
            <li><a href="#"  data-anchor="contact" data-in="4"><span></span></a>
                <div class="fp-tooltip right">Contact</div>
            </li>

        </ul>
    </div>
</div>
<div class="rev_send_pop">
    <p class="rev_send_text">
        <?= $this->lang == 'en' ? 'Thank you for your Review.' : 'Děkujeme za recenzi.'?>
    </p>
</div>
<script src="http://code.jquery.com/jquery-1.12.3.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="<?=$baseurl?>/assets/javascript/jquery.singlefull.js"></script>


<script type="text/javascript">
$(document).ready(function() {
    var GlobalWidth = $(window).width();
    if(GlobalWidth < 425){
        console.log('GlobalWidth mob',GlobalWidth);
        $("#single").singlefull({
            speed: 500,
            loopScroll:true,
            loopTop:true,
            loopBottom:true
        });
// Just a javascript alignment to the content
        function alignContent() {
            var windowHeight = $(window).height();

            $('.content-resizer').each(function() {
                contentHeight = $(this).height();
                $(this).css('top', (windowHeight / 2) - (contentHeight / 2));
            });


        }

// Execute the function
        alignContent();

// Bind the function to the window.onresize
        $(window).bind("resize", function() {
            alignContent();
        });
        $('.g_lis').css({
            'display':'none'
        })
    }

    if(GlobalWidth > 425) {
        var myFullpage = new fullpage('#fullpage', {
            // sectionsColor: ['#1bbc9b', '#4BBFC3', '#7BAABE','',''],
            anchors: ['firstPage', 'secondPage', '3rdPage', '4rdPage', '5rdPage'],
            menu: '#menu',
            continuousVertical: true,
            navigation: true,

            afterLoad: function (anchorLink, index) {
                console.log("AFTER LOAD - anchorLink:" + anchorLink + " index:" + index);
            },
            onLeave: function (index, nextIndex, direction) {

                var GlobalHeight = $(window).height();
                var actPage = $('.g_lis li').find('.active');
                var pageIndex = actPage.attr('data-in');
                $('.rest_menus').removeClass('restaurant_sub_head_active');
                var headData = $('.rest_menus[data-ind=' + pageIndex + ']').addClass('restaurant_sub_head_active');

                if (direction == "down") {
                    console.log("ONLEAVE - index:" + index + " nextIndex:" + nextIndex + " direction:" + direction);
                    var MyWidth = $('.land_cont').width();
                    var myTokos = 255 * 100 / MyWidth;
                    var opsTokos = 635 * 100 / MyWidth;
                    var opTokos = (100 - myTokos) / 2;
                    var textTokos = (100 - opsTokos) / 2;
                    console.log('GlobalHeight',);

                    $('.imgefect').css({
                        'margin-left': '0px'
                    });
                    $('.chef_title').css({
                        'text-align': 'center',
                        'opacity': '1'
                    });
                    $('.chef_text').css({
                        'margin-left': textTokos + '%'
                    });
                    $('.chef_name').css({
                        'opacity': '1'
                    });
                    $('.chef_cit').css({
                        'opacity': '1'
                    });
                    $('.chef_img').css({
                        'border': '10px solid #514c42'
                    });
                }
            },
        });

    }



        $('.vol_min').click(function () {
            $(this).css({'display': 'none'});
            $('.vol_pl').css({'display': 'block'})
            document.getElementById('player_audio').play();
        })
        $('.vol_pl').click(function () {
            $(this).css({'display': 'none'});
            $('.vol_min').css({'display': 'block'})
            document.getElementById('player_audio').pause();
        })
    })
    $( window ).scroll(function() {

    });
    var GlobalHeight = $(window).height();
    var GlobalWidth = $(window).width();

    if(GlobalWidth > 425){
        console.log('GlobalWidth pc',GlobalWidth);
        $( window ).scroll(function() {
            var actPage = $('.g_lis li').find('.active');
            var pageIndex = actPage.attr('data-in');
            $('.rest_menus').removeClass('restaurant_sub_head_active');
            var headData = $('.rest_menus[data-ind='+pageIndex+']').addClass('restaurant_sub_head_active');

            var scroling = $(document).scrollTop();

            var MyWidth = $('.land_cont').width();
            var myTokos = 255*100/MyWidth;
            var opsTokos = 635*100/MyWidth;
            var opTokos = (100-myTokos)/2;
            var textTokos = (100-opsTokos)/2;
            if(scroling == GlobalHeight){
                $('.chef_img').css({
                    'margin-left':opTokos+'%'
                });
                $('.chef_title').css({
                    'text-align':'center',
                    'opacity':'1'
                });
                $('.chef_text').css({
                    'margin-left':textTokos+'%'
                });
                $('.chef_name').css({
                    'opacity':'1'
                });
                $('.chef_cit').css({
                    'opacity':'1'
                });
            }

        });
    }
</script>