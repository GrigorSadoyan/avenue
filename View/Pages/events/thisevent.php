<section>
    <div class="menuItemNone">
        <div class="content">
            <div class="arc_mins">
                <div class="col-lg-12 clear nopadd">
                    <div class="col-lg-4  col-md-4  col-sm-4 col-xs-12 nopadd clear">
                        <div class="tarc_slide_main">
                            <?php foreach ($params['result']['gall'] as $gall){  ?>
                                <div class="tarc_slide_items">
                                    <a class="grouped_elements" rel="group1" href="<?=$baseurl?>/assets/images/content/events/EventGallery/<?=$gall['image']?>">
                                        <img src="<?=$baseurl?>/assets/images/content/events/EventGallery/<?=$gall['image']?>">
                                    </a>
                                </div>
                            <?php  } ?>
                        </div>
                    </div>
                    <div class="col-lg-8  col-md-8  col-sm-8 col-xs-12 nopadd clear">
                        <div class="this_arc_info">
                            <p class="this_archive_inf_name"><?=$params['result']['name_'.$this->lang]?></p>
                            <p class="this_archive_inf_date"><span><?=$params['result']['data']?></span></p>
                            <div class="this_archive_inf_text">
                                <?=$params['result']['text_'.$this->lang]?>
                            </div>
                        </div>
                        <p class="this_arc_back">
                            <a href="<?=$baseurl?>/events/archive/"><span>BACK TO EVENTS</span></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="mob_events">
        <div class="content">
            <div class="col-xs-12 nopadd clear">
                <div class="this_arc_info">
                    <p class="this_archive_inf_name"><?=$params['result']['name_'.$this->lang]?></p>
                    <p class="this_archive_inf_date"><span><?=$params['result']['data']?></span></p>
                    <div class="this_archive_inf_text">
                        <?=$params['result']['text_'.$this->lang]?>
                    </div>
                </div>
            </div>
        </div>
            <div class="col-xs-12 nopadd clear">
                <div class="tarc_slide_main">
                    <?php foreach ($params['result']['gall'] as $gall){  ?>
                        <div class="tarc_slide_items">
                            <img src="<?=$baseurl?>/assets/images/content/events/EventGallery/<?=$gall['image']?>">
                        </div>
                    <?php  } ?>
                </div>
            </div>
            <p class="this_arc_back">
                <a href="<?=$baseurl?>/events/archive/"><span>BACK TO EVENTS</span></a>
            </p>

    </div>


</section>
<script>
    $("a.grouped_elements").attr("rel", "group1").fancybox();

</script>