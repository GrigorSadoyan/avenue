<section>
    <div class="content menuItemNone">
        <div class="clear arc_min">
            <?php  for ($i = 0; $i < count($params['result']); $i++){  ?>
                <?php if($i%2 ==0){ ?>
                    <div class="col-lg-12 clear nopadd archive_main">
                        <div class="col-lg-6 col-md-6 col-sm-6 leftNopadd clear">
                            <div class="archive_big_img">
                                <img src="<?=$baseurl?>/assets/images/content/events/<?=$params['result'][$i]['image']?>" alt="<?=$params['result'][$i]['name_'.$this->lang]?>"/>
                            </div>
                            <div class="right archive_rows_left"></div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 rightNopadd">
                            <div class="archive_info">
                                <div class="archive_infs">
                                    <p class="archive_inf_date"><?=$params['result'][$i]['data']?></p>
                                    <p class="archive_inf_name"><?=$params['result'][$i]['name_'.$this->lang]?></p>
                                    <p class="archive_inf_name_sur"><?=$params['result'][$i]['description_'.$this->lang]?></p>
                                    <p class="archive_inf_more">
                                        <a href="<?=$baseurl?>/events/<?=$params['pages']?>/<?=$params['result'][$i]['id']?>/"><span> <?= $this->lang == 'en' ? 'READ MORE' : 'PŘEČTĚTE SI VÍCE' ?></span></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php  }else{ ?>
                    <div class="col-lg-12 clear nopadd archive_main">
                        <div class="col-lg-6 col-md-6 col-sm-6 leftNopadd clear">
                            <div class="archive_info">
                                <div class="archive_infs">
                                    <p class="archive_inf_date"><?=$params['result'][$i]['data']?></p>
                                    <p class="archive_inf_name"><?=$params['result'][$i]['name_'.$this->lang]?></p>
                                    <p class="archive_inf_name_sur"><?=$params['result'][$i]['description_'.$this->lang]?></p>
                                    <p class="archive_inf_more">
                                        <a href="<?=$baseurl?>/events/<?=$params['pages']?>/<?=$params['result'][$i]['id']?>/"><span><?= $this->lang == 'en' ? 'READ MORE' : 'PŘEČTĚTE SI VÍCE' ?></span></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 rightNopadd">
                            <div class="left archive_rows_right"></div>
                            <div class="archive_big_img">
                                <img src="<?=$baseurl?>/assets/images/content/events/<?=$params['result'][$i]['image']?>" alt="<?=$params['result'][$i]['name_'.$this->lang]?>"/>
                            </div>
                        </div>
                    </div>
                <?php  } ?>
            <?php  } ?>
        </div>
    </div>
    <div class="content mob_button">
        <div class="clear arc_min">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php  for ($i = 0; $i < count($params['result']); $i++){  ?>
                        <div class="col-lg-12 clear nopadd archive_main swiper-slide">
                            <div class="col-lg-6 col-md-6 col-sm-6 clear">
                                <div class="archive_big_img">
                                    <img src="<?=$baseurl?>/assets/images/content/events/<?=$params['result'][$i]['image']?>" alt="<?=$params['result'][$i]['name_'.$this->lang]?>"/>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="archive_info">
                                    <div class="archive_infs">
                                        <p class="archive_inf_date"><?=$params['result'][$i]['data']?></p>
                                        <p class="archive_inf_name"><?=$params['result'][$i]['name_'.$this->lang]?></p>
                                        <p class="archive_inf_name_sur"><?=$params['result'][$i]['description_'.$this->lang]?></p>
                                        <p class="archive_inf_more">
                                            <a href="<?=$baseurl?>/events/<?=$params['pages']?>/<?=$params['result'][$i]['id']?>/"><span>READ MORE</span></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php  } ?>
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
        </div>
    </div>
</section>
<script>
    var swiper = new Swiper('.swiper-container', {
        scrollbar: {
            el: '.swiper-scrollbar',
            hide: true,
        },
    });
</script>