<div class="gal_main gal_images ">
    <?php
//    var_dump(count($params['result']));die;
        if(count($params['result']) == 0) {
    ?>
            <p class="under_construction"><?= $this->lang == 'en' ? 'This PAge Is Under Construction' : 'TATO STRÁNKA SE PŘIPRAVUJE' ?></p>
    <?php
        }else{
    ?>
            <?php  foreach ($params['result'] as $val){ ?>
                <div class="gal_items">
                    <img src="<?=$baseurl?>/assets/images/content/gallery/<?=$val['image']?>" />
                </div>
            <?php  } ?>

    <?php
        }
    ?>

</div>
<script>
    var GlobalHeight = $(window).height();
    var GlobalHeaderHeight = 70;
    var GlobalPageHeight = GlobalHeight - GlobalHeaderHeight;
    $('.gal_main').height(GlobalPageHeight);
    $('.gal_items').height(GlobalPageHeight);
    $('.gal_images').slick({
        autoplay: true,
        speed:500,
        autoplaySpeed: 2000,
        prevArrow:"<div class='a_left_home'><i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i></div>",
        nextArrow:"<div class='a_right_home'><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></div>"
    });
</script>