<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">


<section>
    <div class="r_main clear">
        <div class="col-lg-12 nopadd">
            <div class="col-lg-6 nopadd">
                <div class="res_map_main">
                    <img src="<?=$baseurl?>/assets/images/content/avenue-tables-scheme-2.png" />
                </div>
            </div>
            <div class="col-lg-6 nopadd">
                <div class="r_right_big">
                    <div class="r_rigt_b_opent">
                        <script type='text/javascript' src='//www.opentable.co.uk/widget/reservation/loader?rid=163515&type=standard&theme=tall&iframe=true&overlay=false&domain=couk&lang=en-GB'></script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="rev_send_pop">
        <p class="rev_send_text">
            <?= $this->lang == 'en' ? 'Your reservation has been send to administrators.' : 'Vaše rezervace byla odeslána správcům.' ?>
        </p>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker4').datetimepicker({
            format: 'YYYY/MM/DD'
        });
        $('#datetimepicker5').datetimepicker({
            format: 'HH:mm'
        });
        $('#datetimepicker6').datetimepicker({
            format: 'HH:mm'
        });
    });
</script>