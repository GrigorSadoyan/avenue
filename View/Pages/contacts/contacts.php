<section>
    <div class="tarc_main menuItemNone">
        <div class="col-lg-12 nopadd">
            <div class="col-lg-6  col-md-6 col-sm-5 nopadd">
                <div class="c_l_main">
                    <p class="c_l_number"> <a href="tel:420601083083"><span>+420 601 083 083</span></a></p>
                    <?php   if($this->lang == 'en'){?>
                    <p class="c_l_desc">
                        Vinohradská 83, Prague 2 120 00 <br/>
                        Monday to Thursday From <b> 11:30 to 00:00</b> <br/>
                        Friday From <b>11:30 to 01:00</b> <br/>
                        Saturday From <b>12:00 to 01:00</b> <br/>
                        Sunday From <b>12:00 to 00:00</b> <br/>
                    </p>
                    <?php   }else{ ?>
                        <p class="c_l_desc">
                            Vinohradská 83, Praha 2 120 00 <br/>
                            Pondělí- Čtvrtek <b> 11:30 - 00:00</b> <br/>
                            Pátek <b> 11:30 - 01:00</b> <br/>
                            Sobota <b> 12:00 - 01:00</b> <br/>
                            Neděle <b> 12:00 - 00:00</b> <br/>
                        </p>
                    <?php   } ?>
                    <p class="c_l_write">
                       <span data-toggle="modal" data-target="#myModal"><?= $this->lang == 'en' ? 'WRITE TO US' : 'KONTAKTUJTE NÁS' ?></span>
                    </p>
                    <p class="c_l_social">
                        <a href="https://facebook.com/avenueprague" target="_blank"><span><i class="fa fa-facebook-f"></i></span></a>
                        <a href="https://www.instagram.com/avenue__prague/" target="_blank"><span><i class="fa fa-instagram"></i></span></a>
<!--                        <a href="#"><span><img src="--><?//=$baseurl?><!--/assets/images/content/main_land/telegram_black.png"></span></a>-->
                        <a href="https://cz.pinterest.com/avenuerestaurant/pins/" target="_blank"><span><i class="fa fa-pinterest-p"></i></span></a>
                        <a href="https://www.tripadvisor.cz/Restaurant_Review-g274707-d14142527-Reviews-Avenue_restaurant_bar-Prague_Bohemia.html" target="_blank"><span><i class="fa fa-tripadvisor"></i></span></a>
                    </p>
                </div>
            </div>
            <div class="col-lg-6  col-md-6 col-sm-7 nopadd">
                <div class="c_r_main">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>






    <div class="c_main  ">
        <div class="col-lg-12 nopadd">

            <div class="col-lg-6  col-md-6 col-sm-5 nopadd">
                <div class="c_l_main">
                    <p class="c_l_number"><a href="tel:420601083083"><span>+420 601 083 083</span></a></p>
                    <?php   if($this->lang == 'en'){?>
                        <p class="c_l_desc">
                            Vinohradská 83, Prague 2 120 00 <br/>
                            Monday to Thursday From <b>11:30 to 00:00</b> <br/>
                            Friday From<b> 11:30 to 01:00</b> <br/>
                            Saturday From<b> 12:00 to 01:00</b> <br/>
                            Sunday From<b> 12:00 to 00:00</b> <br/>
                        </p>
                    <?php   }else{ ?>
                        <p class="c_l_desc">
                            Vinohradská 83, Praha 2 120 00 <br/>
                            Pondělí- Čtvrtek <b> 11:30 - 00:00</b> <br/>
                            Pátek <b> 11:30 - 01:00</b> <br/>
                            Sobota <b> 12:00 - 01:00</b> <br/>
                            Neděle <b> 12:00 - 00:00</b> <br/>
                        </p>
                    <?php   } ?>
                    <p class="c_l_write">
                        <span data-toggle="modal" data-target="#myModal"><?= $this->lang == 'en' ? 'WRITE TO US' : 'KONTAKTUJTE NÁS' ?></span>
                    </p>
                    <p class="c_l_social">
                        <a href="https://facebook.com/avenueprague" target="_blank"><span><i class="fa fa-facebook-f"></i></span></a>
                        <a href="https://www.instagram.com/avenue__prague/" target="_blank"><span><i class="fa fa-instagram"></i></span></a>
<!--                        <a href="#"><span><img src="--><?//=$baseurl?><!--/assets/images/content/main_land/telegram_black.png"></span></a>-->
                        <a href="https://cz.pinterest.com/avenuerestaurant/pins/" target="_blank"><span><i class="fa fa-pinterest-p"></i></span></a>
                        <a href="https://www.tripadvisor.cz/Restaurant_Review-g274707-d14142527-Reviews-Avenue_restaurant_bar-Prague_Bohemia.html" target="_blank"><span><i class="fa fa-tripadvisor"></i></span></a>
                    </p>
                </div>
            </div>
            <div class="col-lg-6  col-md-6 col-sm-7 nopadd">
                <div class="c_r_main">
                    <div id="maps"></div>
                </div>
            </div>
        </div>

    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="c_write_mains">
            <p class="c_write_close"><img src="<?=$baseurl?>/assets/images/content/c_close.png" data-dismiss="modal"></p>
            <p class="c_write_title"><span><?= $this->lang == 'en' ? 'WRITE TO US' : 'KONTAKTUJTE NÁS' ?></span></p>
            <p class="c_write_title_row"></p>
            <div class="c_write_form">
                <p><input class="wr_name" type="text" placeholder="<?= $this->lang == 'en' ? 'Name' : 'Jméno'?>*"></p>
                <p><input class="wr_phone" type="text" placeholder="<?= $this->lang == 'en' ? 'Phone' : 'Mobil' ?>*"></p>
                <p><input class="wr_email" type="text" placeholder="<?= $this->lang == 'en' ? 'E-Mail' : 'E-mail' ?>*"></p>
                <p><textarea class="wr_text" rows="6" name="name" placeholder="<?= $this->lang == 'en' ? 'Message' : 'Zpráva' ?>*"></textarea></p>
            </div>
            <p class="c_write_button __send_wr"><input type="submit" value="<?= $this->lang == 'en' ? 'SEND' : 'ODESLAT '?>"></p>
        </div>
    </div>
    <div class="rev_send_pop">
        <p class="rev_send_text">
            <?= $this->lang == 'en' ? 'Thank you.' : 'Děkuji.'?>
        </p>
    </div>
</section>
<script>
    function initMap() {
        var icon = {
            icon:base + "/assets/images/mel_map_pin8.png"
        };
        var uluru = {lat:50.0771814, lng:14.4465884};
        var GlobalWidth = $(window).width();
        if(GlobalWidth <= 425){
            var map = new google.maps.Map(document.getElementById('maps'), {
                zoom: 15,
                center: uluru
            });
        }else{
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: uluru
            });
        }

        var marker = new google.maps.Marker({
            position: uluru,
            map: map,
            icon: icon.icon,
        });
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6kwbUaYPPDJltTBXnLwEwVgvrklMBKkg&callback=initMap">
</script>