<section>
    <div class="col-lg-12 clear nopadd menu_main ">
        <div class="col-lg-3 col-md-3 col-sm-6 nopadd menu_items_main">
            <a href="<?=$baseurl?>/menu/kitchen/1/">
                <div class="menu_items">
                    <img src="<?=$baseurl?>/assets/images/content/menu_kitchen.jpg" alt="Restaurant Avenue Kitchen">
                </div>
                <p class="menu_items_name">
                    <?= $this->lang == 'en' ? 'KITCHEN' : 'KUCHYNĚ' ?>
                </p>
                <div class="mi_black"></div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 nopadd menu_items_main">
            <a href="<?=$baseurl?>/menu/bar/2/">
                <div class="menu_items">
                    <img src="<?=$baseurl?>/assets/images/content/menu_bar.jpg" alt="Restaurant Avenue Bar">
                </div>
                <p class="menu_items_name">
                    <?= $this->lang == 'en' ? 'BAR' : 'BAR' ?>
                </p>
                <div class="mi_black"></div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 nopadd menu_items_main">
            <a href="<?=$baseurl?>/menu/winelist/3/">
                <div class="menu_items">
                    <img src="<?=$baseurl?>/assets/images/content/menu_wine.jpg" alt="Restaurant Avenue Wine">
                </div>
                <p class="menu_items_name">
                    <?= $this->lang == 'en' ? 'WINE LIST' : 'VINNÝ LÍSTEK' ?>
                </p>
                <div class="mi_black"></div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 nopadd menu_items_main">
            <a href="<?=$baseurl?>/menu/cocktails/4/">
                <div class="menu_items">
                    <img src="<?=$baseurl?>/assets/images/content/menu_cocktail.jpg" alt="Restaurant Avenue Cocktail">
                </div>
                <p class="menu_items_name">
                    <?= $this->lang == 'en' ? 'COCKTAILS' : 'Koktejly' ?>
                </p>
                <div class="mi_black"></div>
            </a>
        </div>
    </div>
</section>