<section>
    <?php // var_dump($this->myPageMenu[1]);die; ?>
    <div class="content clear  menuItemNone">
        <div class="col-lg-12 nopadd clear mi_mains">
            <div class="col-lg-6 col-md-6 col-sm-12 leftNopadd">
                <div class="nopadd mi_left_main clear">
                    <div class="mi_left_big">
                        <?php if($this->myPageMenu[1] == 'kitchen'){  ?>
                            <img src="<?=$baseurl?>/assets/images/content/mi_big.jpg">
                        <?php   }elseif($this->myPageMenu[1] == 'bar'){ ?>
                            <img src="<?=$baseurl?>/assets/images/content/menu_bar.jpg">
                        <?php   }elseif($this->myPageMenu[1] == 'winelist'){ ?>
                            <img src="<?=$baseurl?>/assets/images/content/menu_wine.jpg">
                        <?php   }elseif($this->myPageMenu[1] == 'cocktails'){ ?>
                            <img src="<?=$baseurl?>/assets/images/content/menu_cocktail.jpg">
                        <?php  } ?>
                    </div>
                    <div class="mi_left_m_main">
                        <ul class="mi_left_m_list">
<!--                            <li><span class="active_menu_baj">Armenian</span><p></p></li>-->
                            <?php foreach ($params['menu_cats'] as $Mcats) { ?>
                                <li><span class="mi_left_js" data-item="<?=$Mcats['id']?>"><?=$Mcats['name_'.$this->lang]?></span><p></p></li>
                            <?php  } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6  col-sm-12 rightNopadd">
                <div class="nopadd mi_right_main">
                    <div class="col-lg-12 nopadd mi_r_its_mains">
                        <?php foreach ($params['menus'] as $menuM) { ?>
                            <div class="col-lg-3 col-md-3 col-sm-3 mi_r_its nopadd">
                                <a href="<?=$baseurl?>/menu/<?=$menuM['url']?>/<?=$menuM['id']?>/">
                                    <p class="mi_r_its_p <?= $params['menu_type'] == $menuM['url'] ? 'active_mi_p' : '' ?> ">
                                        <?=$menuM['name_'.$this->lang]?>
                                    </p>
                                </a>
                            </div>
                        <?php  } ?>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-10 all_menu nopadd">
                        <?php foreach ($params['menu_all'] as $val){  ?>
                            <div class="mi_r_items" data-bigItem="<?=$val['id']?>">
                                <p class="mi_r_title">
                                    <?=$val['name_'.$this->lang]?>
                                </p>
                                <?php foreach ($val['sub_menu'] as $vals){  ?>
                                    <?php if($vals['block'] == '0'){ ?>
                                        <p class="mi_rThis_item">
                                            <?=$vals['name_'.$this->lang]?>
                                            <span><?=$vals['price']?> Kč</span>
                                        </p>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        <?php  } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content clear  mob_menuItem pcnone">
        <div class="col-xs-12 nopadd">
            <div class="mi_r_its_mains">
                <?php foreach ($params['menus'] as $menuM) { ?>
                    <div class="col-lg-3 col-md-3 col-sm-3 mi_r_its nopadd">
                        <a href="<?=$baseurl?>/menu/<?=$menuM['url']?>/<?=$menuM['id']?>/">
                            <p class="mi_r_its_p <?= $params['menu_type'] == $menuM['url'] ? 'active_mi_p' : '' ?> ">
                                <?=$menuM['name_'.$this->lang]?>
                            </p>
                        </a>
                    </div>
                <?php  } ?>
            </div>
        </div>
        <div class="col-xs-12 nopadd clear">
            <div class="nopadd mi_left_main clear">
                <div class="mi_left_big">
                    <?php if($this->myPageMenu[1] == 'kitchen'){  ?>
                        <img src="<?=$baseurl?>/assets/images/content/mi_big.jpg">
                    <?php   }elseif($this->myPageMenu[1] == 'bar'){ ?>
                        <img src="<?=$baseurl?>/assets/images/content/menu_bar.jpg">
                    <?php   }elseif($this->myPageMenu[1] == 'winelist'){ ?>
                        <img src="<?=$baseurl?>/assets/images/content/menu_wine.jpg">
                    <?php   }elseif($this->myPageMenu[1] == 'cocktails'){ ?>
                        <img src="<?=$baseurl?>/assets/images/content/menu_cocktail.jpg">
                    <?php  } ?>
                </div>
                <div class="mi_left_m_main">
                    <ul class="mi_left_m_list">
                        <!--                            <li><span class="active_menu_baj">Armenian</span><p></p></li>-->
                        <?php foreach ($params['menu_cats'] as $Mcats) { ?>
                            <li><span><?=$Mcats['name_'.$this->lang]?></span><p></p></li>
                        <?php  } ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-12 nopadd clear mob_menu_iTop">
            <?php foreach ($params['menu_all'] as $val){  ?>
                <div class="mi_r_items">
                    <p class="mi_r_title">
                        <?=$val['name_'.$this->lang]?>
                    </p>
                    <?php foreach ($val['sub_menu'] as $vals){  ?>
                        <p class="mi_rThis_item">
                            <?=$vals['name_'.$this->lang]?>
                            <span><?=$vals['price']?>  Kč</span>
                        </p>
                    <?php } ?>
                </div>
            <?php  } ?>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        var items = $('.mi_r_items');

        $('.mi_left_js').click(function () {
            for(var i=0;i<items.length;i++){
                $(items[i]).removeClass('item_active_menu');
                if($(items[i]).attr('data-bigItem') == $(this).attr('data-item')){
                    console.log(items[i]);
                    $('.all_menu').prepend(items[i]);
                    $(items[i]).addClass('item_active_menu')

                }
            }

        })
    })
</script>