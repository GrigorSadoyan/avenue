<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
use Model\Events as EventsModel;
use Model\EventGallery;

class Eventsselect extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'eventsselect') {
                $this->index();
            }elseif ($countRoute == 2 && $route[0] == 'eventsselect' && is_string($route[1])) {
                $this->item($route[1]);
            }elseif($countRoute == 3 && $route[0] == 'eventsselect' && is_string($route[1]) && $route[2] == 'add'){
                $this->thisItem($route[1]);
            }elseif ($countRoute == 3 && $route[0] == 'eventsselect'  && is_string($route[1]) && is_numeric($route[2])) {
                $this->updItemPage($route[2]);
            }elseif($countRoute == 4 && $route[0] == 'eventsselect'  && $route[1] == 'addgal' && $route[2] == 'addgalpic'  && is_numeric($route[3])){
                $this->addGals($route[3]);
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if ($countRoute == 3 && $route[0] == 'eventsselect' && is_string($route[1]) && $route[2] == 'add'){
                $this->addItems($route[1]);
            }elseif ($countRoute == 3 && $route[0] == 'eventsselect' && is_string($route[1]) && is_numeric($route[2])) {
                $this->UpdateItems($route[1],$route[2]);
            }elseif ($countRoute == 2 && $route[0] == 'eventsselect' && $route[1] == 'delete') {
                $this->DeleteItems();
            }elseif($countRoute == 4 && $route[0] == 'eventsselect'  && $route[1] == 'addgal' && $route[2] == 'addgalpic'  && is_numeric($route[3])){
                $this->addGalsP($route[3]);
            }elseif($countRoute == 4 && $route[0] == 'eventsselect'  && $route[1] == 'addgal' && $route[2] == 'addgalpic'  && $route[3] == 'delete'){
                $this->deleteGalsP($route[3]);
            }elseif($countRoute == 2 && $route[0] == 'eventsselect' && $route[1] == 'sort'){
                $this->SortItem();
            }elseif($countRoute == 3 && $route[0] == 'eventsselect'  && $route[1] == 'addgal' && $route[2] == 'sort'){
                $this->SortItemGallery();
            }
        }
    }

    private function index()
    {

        $this->renderView("Pages/eventsselect", "eventsselect", $this->result);
    }
    private function item( $id = false)
    {
        if($id == 'upcoming'){
            $ids = '1';
        }elseif($id == 'archive'){
            $ids = '2';
        }

        $_oEvents = new EventsModel();
        $aEvents = $_oEvents->findByName(array('fild_name'=>'type','fild_val'=>$ids,'order'=>array('asc'=>'ord')));
        $this->result['event_type'] = $id;
        $this->result['result']=$aEvents;

        $this->renderView("Pages/events","events",$this->result);
    }
    private function thisItem($page){
        if($page == 'upcoming'){
            $type = '1';
        }elseif($page == 'archive'){
            $type = '2';
        }
        $this->result['event_type']=$type;
        $this->renderView("Pages/eventsinfo","eventsinfo",$this->result);
    }

    private function updItemPage($id){
        $_oEvents = new EventsModel();
        $aEvents = $_oEvents->findById($id);
        $this->result['result']=$aEvents;
        $this->renderView("Pages/eventsinfo","eventsinfo",$this->result);
    }

    private function addItems($page){

        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath('../assets/images/content/events/');
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unset($_POST['real_img']);
            }else{
                $_POST['image'] = $_POST['real_img'];
                unset($_POST['real_img']);
            }
        }

        $_oEvents = new EventsModel();
        $_oEvents->_post=$_POST;
        $lastId = $_oEvents->insert();
        $url = "eventsselect/$page";
        $this->headreUrl($url);
    }
    private function UpdateItems($page,$id){
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath('../assets/images/content/events/');
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                $filename = '../assets/images/content/events/'.$_POST['real_img'];
                unlink($filename);
                unset($_POST['real_img']);
            }

        }else{
            $_POST['image'] = $_POST['real_img'];
            unset($_POST['real_img']);
        }
        $_oEvents = new EventsModel();
        $_oEvents->_put = $_POST;
        $_oEvents->setId($id);
        $_oEvents->update();
        $url = "eventsselect/$page";
        $this->headreUrl($url);
    }
    private function DeleteItems(){
        $id = $_POST['id'];
        $_oEvents = new EventsModel();
        $aPartnersModel = $_oEvents->findById($id);
        $aImg = $aPartnersModel['image'];
        $filename = '../assets/images/content/events/'.$aImg;
        unlink($filename);
        $_oEvents->delFildName = 'id';
        $_oEvents->delValue = $id;
        $_oEvents->delete();
        echo json_encode(array('error'=>true));
    }
    private function addGals($id){
        $oEventGallery = new EventGallery();
        $aEventGallery = $oEventGallery->findByName(array('fild_name'=>'eid','fild_val'=>$id,'order'=>array('asc'=>'ord')));
        $this->result['result']=$aEventGallery;
        $this->renderView("Pages/eventsgall","eventsgall",$this->result);
    }
    private function addGalsP($id){
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath('../assets/images/content/events/EventGallery/');
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                $filename = '../assets/images/content/events/EventGallery/'.$_POST['real_img'];
                unlink($filename);
                unset($_POST['real_img']);
            }

        }else{
            $_POST['image'] = $_POST['real_img'];
            unset($_POST['real_img']);
        }
        $oEventGallery = new EventGallery();
        $_POST['eid'] = $id;
        $oEventGallery->_post=$_POST;
        $oEventGallery->insert();
        $url = "eventsselect/addgal/addgalpic/$id";
        $this->headreUrl($url);
    }
    private function deleteGalsP(){
        $id = $_POST['id'];
        $oEventGallery = new EventGallery();
        $aEventGallery = $oEventGallery->findById($id);
        $aImg = $aEventGallery['image'];
        $filename = '../assets/images/content/events/EventGallery/'.$aImg;
        unlink($filename);
        $oEventGallery->delFildName = 'id';
        $oEventGallery->delValue = $id;
        $oEventGallery->delete();
        echo json_encode(array('error'=>true));
    }

    private function SortItem(){
        $mBlog = new EventsModel();
        $ords = explode(',',$_POST['ord']);
        foreach ($ords as $key => $value) {
            $idVal = explode('_',$value);
            $mBlog->_put = array(
                'ord'=>$key
            );
            $mBlog->setId($idVal[1]);
            $mBlog->update();
        }
        echo json_encode(array('error'=>true));
    }

    private function SortItemGallery(){
        $mBlog = new EventGallery();
        $ords = explode(',',$_POST['ord']);
        foreach ($ords as $key => $value) {
            $idVal = explode('_',$value);
            $mBlog->_put = array(
                'ord'=>$key
            );
            $mBlog->setId($idVal[1]);
            $mBlog->update();
        }
        echo json_encode(array('error'=>true));
    }
}
