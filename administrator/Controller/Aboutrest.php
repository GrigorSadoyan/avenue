<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
use Model\AboutRestaurant;

class Aboutrest extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'aboutrest') {
                $this->index();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if ($countRoute == 1 && $route[0] == 'aboutrest'){
                $this->editItems();
            }
        }
    }

    private function index()
    {
        $_oAboutRestaurant = new AboutRestaurant();
        $_aAboutRestaurant = $_oAboutRestaurant->findById('1');
        $this->result['result'] = $_aAboutRestaurant;
        $this->renderView("Pages/aboutrest", "aboutrest", $this->result);
    }

    private function editItems(){
        $_oAboutRestaurant = new AboutRestaurant();
        $_oAboutRestaurant->_put = $_POST;
        $_oAboutRestaurant->setId('1');
        $_oAboutRestaurant->update();

        $url = "aboutrest";
        $this->headreUrl($url);
    }
}

