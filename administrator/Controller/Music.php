<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
use Model\Music as MusicM;

class Music extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'music') {
                $this->index();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if ($countRoute == 1 && $route[0] == 'music'){
                $this->addItems();
            }elseif ($countRoute == 2 && $route[0] == 'music' && $route[1] == 'delete'){
                $this->VideoDelete();
            }
        }
    }

    private function index()
    {
        $_oMusic = new MusicM();
        $_aMusic = $_oMusic->findAll(array());
        $this->result['result'] = $_aMusic;
        $this->renderView("Pages/music", "music", $this->result);
    }

    private function VideoDelete(){
        $id = $_POST['id'];
        $_oMusic = new MusicM();
        $_aMusic = $_oMusic->findById($id);
        $mus = $_aMusic['music'];
        $filename = '../assets/music/'.$mus;
        unlink($filename);
        $_oMusic = new MusicM();
        $_oMusic->delFildName = 'id';
        $_oMusic->delValue = $id;
        $_oMusic->delete();
        echo json_encode(array('error'=>true));
    }
    private function addItems(){

        if (isset($_FILES['music']) && $_FILES['music'] != '') {

            if($_FILES["music"]["type"] == 'audio/mp3') {

                if ($_FILES['music']['error'] != 0) {
                    var_dump("error file");die;
                    $err_some = setcookie("error", "Error Image format", time()+20, "/");
                    if($err_some){
                        setcookie("ErrorCv", "10010101", time()+5, "/");
                        header("Location:$this->baseurl/");
                    }
                }
//                var_dump($_FILES);die;
                $dir = "../assets/music/".$_FILES['music']['name'];
                $_POST['music'] = $_FILES['music']['name'];
                $ok = move_uploaded_file($_FILES["music"]["tmp_name"],$dir);
                if($ok){
                    $_oMusic = new MusicM();
                    $_oMusic->_post=$_POST;
                    $ok_insert = $_oMusic->insert();
                    if($ok_insert){
                        setcookie("NoCvError", "10010110", time()+8, "/");
                        header("Location:$this->baseurl/music/");
                    }
                }
            }
        }
    }
}
