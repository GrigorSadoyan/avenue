<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
use Model\ChefText;

class Chef extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'chef') {
                $this->index();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if ($countRoute == 1 && $route[0] == 'chef'){
                $this->editItems();
            }
        }
    }

    private function index()
    {
        $_oChefText = new ChefText();
        $_aChefText = $_oChefText->findById('1');
        $this->result['result'] = $_aChefText;
        $this->renderView("Pages/chef", "chef", $this->result);
    }

    private function editItems(){
        $_oChefText = new ChefText();
        $_oChefText->_put = $_POST;
        $_oChefText->setId('1');
        $_oChefText->update();

        $url = "chef";
        $this->headreUrl($url);
    }
}

