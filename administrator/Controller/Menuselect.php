<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
use Model\Menu;
use Model\Menucats;
use Model\MenuItems;

class Menuselect extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'menuselect') {
                $this->index();
            } elseif ($countRoute == 2 && $route[0] == 'menuselect' && is_numeric($route[1])) {
                $this->thisMenuCategorie($route[1]);
            }elseif ($countRoute == 3 && $route[0] == 'menuselect' && $route[1] == 'editcat'  && is_numeric($route[2]) ) {
                $this->thisMenuCategorieEditPage($route[2]);
            } elseif ($countRoute == 3 && $route[0] == 'menuselect' && is_numeric($route[1]) && $route[2] == 'add'  ) {
                $this->AddMenuCat($route[1]);
            } elseif ($countRoute == 3 && $route[0] == 'menuselect' && is_numeric($route[1]) && $route[2] == 'thisitems'  ) {
                $this->ThisItems($route[1]);
            } elseif ($countRoute == 4 && $route[0] == 'menuselect' && is_numeric($route[1]) && $route[2] == 'thisitems'  && $route[3] == 'add'  ) {
                $this->AddMenuItem($route[1]);
            }elseif ($countRoute == 3 && $route[0] == 'menuselect'  && $route[1] == 'thisitems' && is_numeric($route[2])) {
                $this->EditMenuItem($route[2]);
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($countRoute == 3 && $route[0] == 'menuselect' && $route[1] == 'editcat'  && is_numeric($route[2])){
                $this->thisMenuCategorieEdit($route[2]);
            }elseif($countRoute == 3 && $route[0] == 'menuselect' && is_numeric($route[1]) && $route[2] == 'add' ){
                $this->AddMenuCatPost($route[2]);
            }elseif($countRoute == 3 && $route[0] == 'menuselect' && $route[1] == 'menucat' && $route[2] == 'delete'){
                $this->DeleteMenuCatPost($route[2]);
            }elseif ($countRoute == 4 && $route[0] == 'menuselect' && is_numeric($route[1]) && $route[2] == 'thisitems'  && $route[3] == 'add'  ) {
                $this->AddMenuItemPost($route[1]);
            }elseif ($countRoute == 3 && $route[0] == 'menuselect'  && $route[1] == 'thisitems' && is_numeric($route[2])) {
                $this->EditMenuItemPost($route[2]);
            }elseif($countRoute == 3 && $route[0] == 'menuselect' && $route[1] == 'thisitems' && $route[2] == 'delete'){
                $this->DeleteMenuItem($route[2]);
            }elseif($countRoute == 3 && $route[0] == 'menuselect' && $route[1] == 'cat' && $route[2] == 'sort'){
                $this->SortItem();
            }elseif($countRoute == 3 && $route[0] == 'menuselect' && $route[1] == 'thisitems' && $route[2] == 'sort'){
                $this->ThisSortItem();
            }elseif($countRoute == 3 && $route[0] == 'menuselect' && $route[1] == 'thisitems' && $route[2] == 'block' ){
                $this->BlockItem();
            }
        }
    }

    private function index()
    {
        $_oMenu = new Menu();
        $_aMenu = $_oMenu->findAll(array());
        $this->result['result'] = $_aMenu;
        $this->renderView("Pages/menuselect", "menuselect", $this->result);
    }
    private function thisMenuCategorie( $id = false)
    {
        $_oMenu = new Menu();
        $_aMenu = $_oMenu->findById($id);
        $this->result['cat_name'] =$_aMenu;

        $_oMenucats = new Menucats();
        $_aMenucats = $_oMenucats->findByName(array('fild_name'=>'mid','fild_val'=>$id,'order'=>array('asc'=>'ord')));
        $this->result['result'] = $_aMenucats;

        $this->renderView("Pages/menucat","menucat",$this->result);
    }
    private function thisMenuCategorieEditPage($id){
        $_oMenucats = new Menucats();
        $_aMenucats = $_oMenucats->findById($id);
        $this->result['result'] = $_aMenucats;

        $this->renderView("Pages/menucatedit","menucatedit",$this->result);
    }

    private function AddMenuCat($id){
        $this->renderView("Pages/menucatedit","menucatedit",$this->result);
    }

    private function AddMenuCatPost(){
        $_oMenucats = new Menucats();
        $_oMenucats->_post=$_POST;
        $lastId = $_oMenucats->insert();

        $_aMenucats = $_oMenucats->findById($lastId);
        $mid = $_aMenucats['mid'];
        $url = "menuselect/$mid";
        $this->headreUrl($url);
    }

    private function DeleteMenuCatPost(){
        $id = $_POST['id'];
        $_oMenucats = new Menucats();
        $_oMenucats->delFildName = 'id';
        $_oMenucats->delValue = $id;
        $_oMenucats->delete();
        echo json_encode(array('error'=>true));
    }
    private function ThisItems($id){
        $_oMenucats = new Menucats();
        $_aMenucats = $_oMenucats->findById($id);
        $this->result['cat_name'] = $_aMenucats;

        $_oMenuItems = new MenuItems();
        $_aMenuItems = $_oMenuItems->findByName(array('fild_name'=>'mcid','fild_val'=>$id,'order'=>array('asc'=>'ord')));
        $this->result['result'] = $_aMenuItems;

        $this->renderView("Pages/menuitem","menuitem",$this->result);
    }
    private function AddMenuItem(){
        $this->renderView("Pages/menuiteminfo","menuiteminfo",$this->result);
    }
    private function AddMenuItemPost($id){
        $_oMenuItems = new MenuItems();
        $_POST['mcid'] = $id;
        $_oMenuItems->_post=$_POST;
        $lastId = $_oMenuItems->insert();

        $_aMenucats = $_oMenuItems->findById($lastId);
        $mid = $_aMenucats['mcid'];
        $url = "menuselect/$mid/thisitems";
        $this->headreUrl($url);
    }
    private function EditMenuItem($id){
        //var_dump($id);die;
        $_oMenuItems = new MenuItems();
        $_aMenuItems = $_oMenuItems->findById($id);

        $this->result['result'] = $_aMenuItems;

        $this->renderView("Pages/menuiteminfo","menuiteminfo",$this->result);
    }
    private function EditMenuItemPost($id){
        $_oMenuItems = new MenuItems();
        $_oMenuItems->_put = $_POST;
        $_oMenuItems->setId($id);
        $_oMenuItems->update();

        $_aMenucats = $_oMenuItems->findById($id);
        $mid = $_aMenucats['mcid'];
        $url = "menuselect/$mid/thisitems";
        $this->headreUrl($url);
    }
    private function DeleteMenuItem(){
        $id = $_POST['id'];
        $_oMenuItems = new MenuItems();
        $_oMenuItems->delFildName = 'id';
        $_oMenuItems->delValue = $id;
        $_oMenuItems->delete();
        echo json_encode(array('error'=>true));
    }







    private function thisMenuCategorieEdit($id){
        $_oMenucats = new Menucats();
        $_oMenucats->_put = $_POST;
        $_oMenucats->setId($id);
        $_oMenucats->update();

        $_aMenucats = $_oMenucats->findById($id);
        $mid = $_aMenucats['mid'];
        $url = "menuselect/$mid";
        $this->headreUrl($url);
    }

    private function SortItem(){
        $mBlog = new Menucats();
        $ords = explode(',',$_POST['ord']);
        foreach ($ords as $key => $value) {
            $idVal = explode('_',$value);
            $mBlog->_put = array(
                'ord'=>$key
            );
            $mBlog->setId($idVal[1]);
            $mBlog->update();
        }
        echo json_encode(array('error'=>true));
    }

    private function ThisSortItem(){
        $mBlog = new MenuItems();
        $ords = explode(',',$_POST['ord']);
        foreach ($ords as $key => $value) {
            $idVal = explode('_',$value);
            $mBlog->_put = array(
                'ord'=>$key
            );
            $mBlog->setId($idVal[1]);
            $mBlog->update();
        }
        echo json_encode(array('error'=>true));
    }

    private function BlockItem(){
        $id = $_POST['id'];
        $_oMenuItems = new MenuItems();
        $_aMenuItems = $_oMenuItems->findById($id);
        //var_dump();die;
        $block = $_aMenuItems['block'];
        $mmm = $_aMenuItems['mcid'];
        if($block == '0'){
            $newData ='1';
        }elseif($block == '1'){
            $newData ='0';
        }
        $_POST['block'] = intval($newData);
        $_oMenuItems->_put = $_POST;
        $_oMenuItems->setId($id);
        $_oMenuItems->update();
        $url = "menuselect/$mmm/thisitems/";
        $this->headreUrl($url);
    }
}
