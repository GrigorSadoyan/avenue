<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
use Model\Gallery as GalleryM;
use Model\GalleryItems;
use Model\GalleryVideo;

class Gallery extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'gallery') {
                $this->index();
            }elseif ($countRoute == 2 && $route[0] == 'gallery' && is_numeric($route[1]) && $route[1] != '4') {
                $this->item($route[1]);
            }elseif ($countRoute == 2 && $route[0] == 'gallery' && is_numeric($route[1]) && $route[1] == '4') {
                $this->itemVideo($route[1]);
            }
//elseif($countRoute == 3 && $route[0] == 'eventsselect' && is_string($route[1]) && $route[2] == 'add'){
//                $this->thisItem($route[1]);
//            }elseif ($countRoute == 3 && $route[0] == 'eventsselect'  && is_string($route[1]) && is_numeric($route[2])) {
//                $this->updItemPage($route[2]);
//            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if ($countRoute == 2 && $route[0] == 'gallery' && is_numeric($route[1]) && $route[1] != '4'){
                $this->addItems($route[1]);
            }elseif ($countRoute == 3 && $route[0] == 'gallery' && $route[1] == 'items' && $route[2] == 'delete'){
                $this->deleteItems($route[1]);
            }elseif ($countRoute == 3 && $route[0] == 'gallery' && $route[1] == 'items' && $route[2] == 'sort'){
                $this->SortItem();
            }elseif ($countRoute == 2 && $route[0] == 'gallery' && is_numeric($route[1]) && $route[1] == '4') {
                $this->itemVideoAdd();
            }elseif ($countRoute == 3 && $route[0] == 'gallery' && is_numeric($route[1]) && $route[1] == '4' && $route[2]== 'delete') {
                $this->itemVideoDelete();
            }elseif ($countRoute == 3 && $route[0] == 'gallery' && $route[1] == 'video' && $route[2] == 'sort'){
                $this->SortVideoItem();
            }
        }
    }

    private function index()
    {
        $_oGallery = new GalleryM();
        $_aGallery = $_oGallery->findAll(array());
        $this->result['result'] = $_aGallery;
        $this->renderView("Pages/gallery", "gallery", $this->result);
    }

    private function item($id = false)
    {
        $_oGallery = new GalleryM();
        $_aGallery = $_oGallery->findById($id);
        $this->result['gal_name'] = $_aGallery;

        $_oGallery = new GalleryItems();
        $_aGallery = $_oGallery->findByName(array('fild_name'=>'gid','fild_val'=>$id,'order'=>array('asc'=>'ord')));
        $this->result['result'] = $_aGallery;
        $this->renderView("Pages/galleryitems", "galleryitems", $this->result);
    }
    private function itemVideo(){
        $_oGalleryVideo = new GalleryVideo();
        $_aGalleryVideo = $_oGalleryVideo->findAll(array('order'=>array('asc'=>'ord')));
        $this->result['result'] = $_aGalleryVideo;
        $this->renderView("Pages/galleryvideos", "galleryvideos", $this->result);
    }
    private function addItems($gid){
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time() + 20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath('../assets/images/content/gallery/');
            $this->uploadData = $this->objImage->upload();
            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unset($_POST['real_img']);
            } else {
                $_POST['image'] = $_POST['real_img'];
                unset($_POST['real_img']);

            }
        }
        $_POST['gid'] = $gid;
        $_oGallery = new GalleryItems();
        $_oGallery->_post=$_POST;
        $_oGallery->insert();
        $url = "gallery/$gid";
        $this->headreUrl($url);
    }
    function deleteItems(){
        $id = $_POST['id'];
        $_oGallery = new GalleryItems();
        $aGalleryItems = $_oGallery->findById($id);

        $aImg = $aGalleryItems['image'];
        $filename = '../assets/images/content/gallery/'.$aImg;
        unlink($filename);
        $_oGallery->delFildName = 'id';
        $_oGallery->delValue = $id;
        $_oGallery->delete();
        echo json_encode(array('error'=>true));
    }

    private function itemVideoAdd(){
        $_oGalleryVideo = new GalleryVideo();
        $_oGalleryVideo->_post=$_POST;
        $_oGalleryVideo->insert();
        $url = "gallery/4";
        $this->headreUrl($url);
    }
    private function itemVideoDelete(){
        $id = $_POST['id'];
        $_oGalleryVideo = new GalleryVideo();
        $_oGalleryVideo->delFildName = 'id';
        $_oGalleryVideo->delValue = $id;
        $_oGalleryVideo->delete();
        echo json_encode(array('error'=>true));
    }
    private function SortItem(){

        $mBlog = new GalleryItems();
        $ords = explode(',',$_POST['ord']);
        foreach ($ords as $key => $value) {
            $idVal = explode('_',$value);
            $mBlog->_put = array(
                'ord'=>$key
            );
            $mBlog->setId($idVal[1]);
            $mBlog->update();
        }
        echo json_encode(array('error'=>true));
    }
    private function SortVideoItem(){
        $mBlog = new GalleryVideo();
        $ords = explode(',',$_POST['ord']);
        foreach ($ords as $key => $value) {
            $idVal = explode('_',$value);
            $mBlog->_put = array(
                'ord'=>$key
            );
            $mBlog->setId($idVal[1]);
            $mBlog->update();
        }
        echo json_encode(array('error'=>true));
    }
}
