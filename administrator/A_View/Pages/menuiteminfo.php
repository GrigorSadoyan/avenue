<div id='content'>

    <form id='main_form' action='' method='post' enctype="multipart/form-data">
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">Page Content</h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box_edit box_ck">

                <div class="form_input">
                    <label><b>Name in <span class="lang_red">English</span></b></label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text input_text_home" name='name_en' placeholder="Name" value='<?= isset($params['result']['name_en']) ? $params['result']['name_en'] : '' ?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label><b>Name in <span class="lang_red">Czech</span></b></label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text input_text_home" name='name_cz' placeholder="Name" value='<?= isset($params['result']['name_cz']) ? $params['result']['name_cz'] : '' ?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label><b>Description in <span class="lang_red">English</span></b></label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <textarea  class="input_text input_text_home" name='description_en' cols="100" rows="6"><?= isset($params['result']['description_en']) ? $params['result']['description_en'] : '' ?></textarea>
                    </div>
                </div>

                <div class="form_input">
                    <label><b>Description in <span class="lang_red">Czech</span></b></label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <textarea  class="input_text input_text_home" name='description_cz' cols="100" rows="6"><?= isset($params['result']['description_cz']) ? $params['result']['description_cz'] : '' ?></textarea>
                    </div>
                </div>

                <div class="form_input">
                    <label>Price</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="number" class="input_text input_text_home" name='price' placeholder="price" value='<?= isset($params['result']['price']) ? $params['result']['price'] : '' ?>'>
                    </div>
                </div>
<!--                --><?php //if(!isset($params['result'])){ ?>
<!--                    --><?php
//                    $url = $_SERVER['HTTP_REFERER'];
//                    $ints = explode('/',$url);
//                    ?>
<!--                    <input type="hidden" class="input_text input_text_home" name='mid' value='--><?//=$ints[5]?><!--'>-->
<!--                --><?php //}  ?>


                <div class="clear"></div>
                <div class="form_input a_form_butt">
                    <div class="input_group clen">
                        <div class="input_img forsave"><i class="fa fa-floppy-o"></i></div>
                        <button class='save' for='main_form'>Save</button>
                    </div>
                </div>
            </div>


        </div>
    </form>
</div>

<script>
    $( function() {
        $( "#datepicker" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>




<style>
    .a_product_img_del {
        position: absolute;
        display: none;
        top: 100px;
        font-size: 50px;
        cursor: pointer;
        z-index: 100;
    }

    .input_group {
        position: relative;
    }
    .foto_block {
        width: 250px;
        height: 150px;
        border: 1px solid;
        background: #eee;
        cursor: pointer;
        position: relative;
    }
    .input_group:hover .a_product_img_del {
        display: block;
    }
    .foto_bloch_lefts{
        float:left;
    }
</style>