<div id='content'>

    <form id='main_form' action='' method='post' enctype="multipart/form-data">

        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title"><?=$params['gal_name']['name_en']?></h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box_edit box_ck">
                    <div class="form_input">
                        <label>Upload Galleri image (.jpg,.png)</label>
                        <div class="input_group">
                            <div class='_foto_block foto_block forempty'>
                                <img src='<?=$baseurlM?>/assets/images/content/gallery/<?= isset($params['result']['image']) && $params['result']['image'] != '' ? $params['result']['image'] : ''?>' />
                                <input type="file" name="image" class="img_file" >
                                <div class='empty_foto'><i class="fa fa-picture-o"></i></div>
                            </div>
                        </div>
                    </div>

                <div class="clear"></div>
                <div class="form_input a_form_butt">
                    <div class="input_group clen">
                        <div class="input_img forsave"><i class="fa fa-floppy-o"></i></div>
                        <button class='save' for='main_form'>SAVE</button>
                    </div>
                </div>
            </div>


            <hr/>

            <div class="gall_img_col clear" id="main_tbody">
                <?php foreach ($params['result'] as $val) { ?>
                    <div class="gal_img_col " id="m_<?=$val['id']?>">
                        <div class="delete_img_gall" data-id="<?=$val['id']?>">X</div>
                        <img src="<?=$baseurlM?>/assets/images/content/gallery/<?=$val['image']?>" alt="">
                    </div>
                <?php }  ?>
            </div>




        </div>
    </form>
</div>
<script>
    $('.delete_img_gall').click(function () {
        if(!confirm("Are you sure delete this item?")){return false;}
        var self = $(this);
        var url = base+"/gallery/items/delete/";
        var id = $(this).data('id');
        var body = "id="+id+"";
        requestPost(url,body,function(){
            if(this.readyState == 4){
                var result = JSON.parse(this.responseText);
                if(result.error){
                    self.parent('.gal_img_col').fadeOut();
                }else{

                }
            }
        })
    })

    $('#main_tbody').sortable({
        update: function () {
            ords = $(this).sortable("toArray");
            console.log($(this).sortable("toArray"))
            var url = base+"/gallery/items/sort/";
            var body = "ord="+ords+"";
            requestPost(url,body,function(){
                if(this.readyState == 4){
//                    var result = JSON.parse(this.responseText);
//                    if(result.error){
//                        self.parent('td').parent('tr').fadeOut();
//                    }else{
//
//                    }
                }
            })
        }
    })
</script>
<style>
    .gal_img_col {
        position: relative;
        width: 250px;
        height: 165px;
        background: #f2f2f2;
        float: left;
        overflow: hidden;
        margin-left: 15px;
        margin-top: 15px;
        margin-bottom: 15px;
    }
    .delete_img_gall {
        position: absolute;
        right: 2px;
        top: 2px;
        z-index: 999;
        font-size: 23px;
        width: 30px;
        height: 30px;
        border-radius: 50%;
        background: #4b6589;
        text-align: center;
        color: #fff;
        cursor: pointer;
    }
    .gal_img_col img {
        width: 100%;
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        margin: auto;
    }
</style>