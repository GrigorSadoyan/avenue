<div id='content'>

    <form id='main_form' action='' method='post' enctype="multipart/form-data">

        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">Music</h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box_edit box_ck">
                <div class="sel_span">Select Only .mp3</div>
                <div class="sel_bl_mp">
                    <i class="fa fa-music" aria-hidden="true"></i>
                </div>
                <div class="sel_res"></div>
                <input type="file" class="select_mp3" name="music" />



                <div class="clear"></div>
                <div class="form_input a_form_butt">
                    <div class="input_group clen">
                        <div class="input_img forsave"><i class="fa fa-floppy-o"></i></div>
                        <button class='save' for='main_form'>SAVE</button>
                    </div>
                </div>
            </div>


            <hr/>

            <div class="gall_img_col clear">
                <?php foreach ($params['result'] as $val) { ?>
                    <div class="gal_vid_main">
                        <div class="gal_img_col vid_gals">
                            <div class="delete_img_gall " data-id="<?=$val['id']?>">X</div>
                            <?=$val['music']?> <br/>
                            <audio id="player_audio" controls>
                                <source src="<?=$baseurlM?>/assets/music/<?=$val['music']?>" type="audio/mpeg">
                            </audio>

                        </div>
                    </div>

                <?php }  ?>
            </div>




        </div>
    </form>
</div>
<script>
    $('.delete_img_gall').click(function () {
        if(!confirm("Are you sure delete this item?")){return false;}
        var self = $(this);
        var url = base+"/music/delete/";
        var id = $(this).data('id');
        var body = "id="+id+"";
        requestPost(url,body,function(){
            if(this.readyState == 4){
                var result = JSON.parse(this.responseText);
                if(result.error){
                    self.parent('div').parent('div').fadeOut();
                }else{

                }
            }
        })
    })
    
    $('.sel_bl_mp').click(function () {
        $('.select_mp3').click();

        $('.select_mp3').change(function () {
            var fileName = this.value;
            fileName = fileName.replace(/.*[\/\\]/, '');
            $('.sel_res').text(fileName);
        })
    })
</script>
<style>
    .sel_span{
        font-size: 16px;
        padding-left: 10px;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    }
    .sel_bl_mp{
        width: 250px;
        border: 1px solid #9a9a9a;
        border-radius: 4px;
        margin: 10px;
        text-align: center;
        font-size: 120px;
        padding-bottom: 15px;
        padding-right: 15px;
        cursor: pointer;
        color: #7d7b7b;
    }
    .select_mp3{
        visibility: hidden;
    }
    audio {
        width: 500px;
        height: 32px;
    }
    .gal_vid_main{
        position: relative;
        border: 1px solid #2b2b2b;
        margin-top: 25px;
    }
    .vid_gals iframe{
        width: 100%!important;
        height: 100%!important;
    }
    .gal_img_col {
        position: relative;
        width: 90%;
        height: 60px;
        padding-top: 10px;
        background: #f2f2f2;
        float: none !important;
        overflow: hidden;
        margin-left: 15px;
        margin-top: 15px;
        margin-bottom: 15px;
    }
    .delete_img_gall {
        position: absolute;
        right: 2px;
        top: 2px;
        z-index: 999;
        font-size: 23px;
        width: 30px;
        height: 30px;
        border-radius: 50%;
        background: #4b6589;
        text-align: center;
        color: #fff;
        cursor: pointer;
    }
    .gal_img_col img {
        width: 100%;
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        margin: auto;
    }
</style>