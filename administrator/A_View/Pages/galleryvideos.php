<div id='content'>

    <form id='main_form' action='' method='post' enctype="multipart/form-data">

        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">Videos</h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box_edit box_ck">
                <div class="form_input">
                    <label><b>Name in <span class="lang_red">English</span></b></label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text input_text_home" name='name_en' placeholder="Name" value='<?= isset($params['result']['name_en']) ? $params['result']['name_en'] : '' ?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label><b>Name in <span class="lang_red">Czech</span></b></label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text input_text_home" name='name_cz' placeholder="Name" value='<?= isset($params['result']['name_cz']) ? $params['result']['name_cz'] : '' ?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label>Video</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-youtube"></i></div>
                        <input type="text" class="input_text input_text_home" name='video' placeholder="Video Tag" value='<?= isset($params['result']['name_cz']) ? $params['result']['name_cz'] : '' ?>'>
                    </div>
                </div>




                <div class="clear"></div>
                <div class="form_input a_form_butt">
                    <div class="input_group clen">
                        <div class="input_img forsave"><i class="fa fa-floppy-o"></i></div>
                        <button class='save' for='main_form'>SAVE</button>
                    </div>
                </div>
            </div>


            <hr/>

            <div class="gall_img_col clear" id="main_tbody">
                <?php foreach ($params['result'] as $val) { ?>
                    <div class="gal_vid_main" id="m_<?=$val['id']?>">
                        <div class="gal_img_col vid_gals">
                            <div class="delete_img_gall " data-id="<?=$val['id']?>">X</div>
                            <?=$val['video']?>
                        </div>
                        <div class="gal_vids_texts">
                            <p><?=$val['name_en']?></p>
                            <p><?=$val['name_cz']?></p>
                        </div>
                    </div>

                <?php }  ?>
            </div>




        </div>
    </form>
</div>
<script>
    $('.delete_img_gall').click(function () {
        if(!confirm("Are you sure delete this item?")){return false;}
        var self = $(this);
        var url = base+"/gallery/4/delete/";
        var id = $(this).data('id');
        var body = "id="+id+"";
        requestPost(url,body,function(){
            if(this.readyState == 4){
                var result = JSON.parse(this.responseText);
                if(result.error){
                    console.log('asdasdsadasdasdasdsada');
                    self.parent('div').parent('div').fadeOut();
                }else{

                }
            }
        })
    })

    $('#main_tbody').sortable({
        axis: "y",
        update: function () {
            ords = $(this).sortable("toArray");
            console.log($(this).sortable("toArray"))
            var url = base+"/gallery/video/sort/";
            var body = "ord="+ords+"";
            requestPost(url,body,function(){
                if(this.readyState == 4){
//                    var result = JSON.parse(this.responseText);
//                    if(result.error){
//                        self.parent('td').parent('tr').fadeOut();
//                    }else{
//
//                    }
                }
            })
        }
    })

</script>
<style>
    .gal_vids_texts{
        position: absolute;
        top: 0px;
        z-index: 1000;
        left: 450px;
    }
    .gal_vid_main{
        position: relative;
        border: 1px solid #2b2b2b;
        margin-top: 25px;
    }
    .vid_gals iframe{
        width: 100%!important;
        height: 100%!important;
    }
    .gal_img_col {
        position: relative;
        width: 350px;
        height: 200px;
        background: #f2f2f2;
        float: none !important;
        overflow: hidden;
        margin-left: 15px;
        margin-top: 15px;
        margin-bottom: 15px;
    }
    .delete_img_gall {
        position: absolute;
        right: 2px;
        top: 2px;
        z-index: 999;
        font-size: 23px;
        width: 30px;
        height: 30px;
        border-radius: 50%;
        background: #4b6589;
        text-align: center;
        color: #fff;
        cursor: pointer;
    }
    .gal_img_col img {
        width: 100%;
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        margin: auto;
    }
</style>