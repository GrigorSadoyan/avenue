<div id='content'>

    <div id='table_div'>
        <div class='table_head'>
            <h3></h3>
        </div>
        <div class='table_head'>
            <h1>Add Content to page Events</h1>
        </div>
        <div class='table_head'>
            <div class="form_input">
                <div class="input_group add_project">
                    <div class="input_img forsave">
                        <i class="fa fa-plus"></i>
                    </div>
                    <a href='<?= $baseurl ?>/eventsselect/<?=$params['event_type']?>/add/' class='save'>Add</a>
                </div>
            </div>
        </div>
        <table>
            <thead>
            <tr>
                <th class='table_num'>#</th>
                <th class='w_10'>Name</th>
                <th class='w_10'>Add Gallery</th>
                <th class='table_action last_th'>Action</th>
            </tr>
            </thead>
            <tbody data-table='' id="main_tbody">
            <?php
            $numbered = 0;

            if (isset($params['result'])) {

                foreach ($params['result'] as $val) {
                    $numbered++
                    ?>
                    <tr id='m_<?= $val['id'] ?>'>
                        <td>
                            <span><?= $numbered ?></span>
                        </td>
                        <td>
                           <span><?= $val['name_en'] ?></span>
                        </td>
                        <td>
                            <a href="<?=$baseurl?>/eventsselect/addgal/addgalpic/<?=$val['id']?>/"><span class="add_gals"><i class="fa fa-camera" aria-hidden="true"></i></span></a>
                        </td>
                        <td class='last_td'>
                            <a href='<?= $baseurl ?>/eventsselect/<?=$params['event_type']?>/<?=$val['id']?>/'><span class='action_td'><i class="fa fa-pencil-square-o"></i></span></a>
                            <span class='action_td action_delete' data-id="<?= $val['id'] ?>" data-get='eventsselect'><i class="fa fa-trash-o"></i></span>
                        </td>
                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>


    </div>
</div>
<style>
    .add_gals{
        font-size: 20px;
    }
    .add_gals:hover{
        color: #514f4e;
        cursor: pointer;

    }
    a{
        color:inherit;
    }
</style>

<script>
    $('#main_tbody').sortable({
        axis: "y",
        update: function () {
            ords = $(this).sortable("toArray");
            console.log($(this).sortable("toArray"))
            var url = base+"/eventsselect/sort/";
            var body = "ord="+ords+"";
            requestPost(url,body,function(){
                if(this.readyState == 4){
//                    var result = JSON.parse(this.responseText);
//                    if(result.error){
//                        self.parent('td').parent('tr').fadeOut();
//                    }else{
//
//                    }
                }
            })
        }
    })
</script>