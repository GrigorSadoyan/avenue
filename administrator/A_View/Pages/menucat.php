<div id='content'>

    <div id='table_div'>
        <div class='table_head'>
            <h3></h3>
        </div>
        <div class='table_head'>
            <h1><?=$params['cat_name']['name_en']?></h1>
        </div>
        <div class='table_head'>
            <div class="form_input">
                <div class="input_group add_project">
                    <div class="input_img forsave">
                        <i class="fa fa-plus"></i>
                    </div>
                    <a href='<?= $baseurl ?>/menuselect/<?=$params['cat_name']['id']?>/add/' class='save'>Add Items</a>
                </div>
            </div>
        </div>
        <table>
            <thead>
            <tr>
                <th class='table_num'>#</th>
                <th class='w_10'>Name</th>

                <th class='table_action last_th'>Action</th>
            </tr>
            </thead>
            <tbody data-table='' id="main_tbody">
            <?php
            $numbered = 0;

            if (isset($params['result'])) {

                foreach ($params['result'] as $val) {
                    $numbered++
                    ?>
                    <tr id='m_<?= $val['id'] ?>'>
                        <td>
                            <span><?= $numbered ?></span>
                        </td>
<!--                        <td>-->
<!--                            <input type="checkbox" class='checkbox_anime sub_chek' id="ch_--><?//= $val['id'] ?><!--"-->
<!--                                   data-get='homes' data-id="--><?//= $val['id'] ?><!--"/>-->
<!--                            <label class='chekbox_label' for="ch_--><?//= $val['id'] ?><!--"></label>-->
<!--                        </td>-->
                        <td>
                            <a href='<?= $baseurl ?>/menuselect/<?= $val['id'] ?>/thisitems/'><span><?= $val['name_en'] ?></span></a>
                        </td>

                        <td class='last_td'>
                            <a href='<?= $baseurl ?>/menuselect/editcat/<?=$val['id']?>/'><span class='action_td'><i class="fa fa-pencil-square-o"></i></span></a>
                            <span class='action_td action_delete' data-id="<?= $val['id'] ?>" data-get='menuselect/menucat'><i class="fa fa-trash-o"></i></span>
                        </td>
                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>


    </div>
</div>

<script>
    $('#main_tbody').sortable({
        axis: "y",
        update: function () {
            ords = $(this).sortable("toArray");
            console.log($(this).sortable("toArray"))
            var url = base+"/menuselect/cat/sort/";
            var body = "ord="+ords+"";
            requestPost(url,body,function(){
                if(this.readyState == 4){
//                    var result = JSON.parse(this.responseText);
//                    if(result.error){
//                        self.parent('td').parent('tr').fadeOut();
//                    }else{
//
//                    }
                }
            })
        }
    })

</script>