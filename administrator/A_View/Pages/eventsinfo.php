<div id='content'>

    <form id='main_form' action='' method='post' enctype="multipart/form-data">
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">Page Content</h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box_edit box_ck">

                <div class="form_input">
                    <label><b>Name in <span class="lang_red">English</span></b></label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text input_text_home" name='name_en' placeholder="Name" value='<?= isset($params['result']['name_en']) ? $params['result']['name_en'] : '' ?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label><b>Name in <span class="lang_red">Czech</span></b></label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text input_text_home" name='name_cz' placeholder="Name" value='<?= isset($params['result']['name_cz']) ? $params['result']['name_cz'] : '' ?>'>
                    </div>
                </div>

                <hr/>
                <div class="form_input">
                    <label><b>Description in <span class="lang_red">English</span></b></label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text input_text_home" name='description_en' placeholder="Description" value='<?= isset($params['result']['description_en']) ? $params['result']['description_en'] : '' ?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label><b>Description in <span class="lang_red">Czech</span></b></label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text input_text_home" name='description_cz' placeholder="Description" value='<?= isset($params['result']['description_cz']) ? $params['result']['description_cz'] : '' ?>'>
                    </div>
                </div>
                <hr/>

                <div class="form_input">
                    <label><b>Text in <span class="lang_red">English</span></b></label>
                    <div class="input_group">
                        <textarea id="editors" class="input_text input_text_home" name='text_en' placeholder="Description"><?= isset($params['result']['text_en']) ? $params['result']['text_en'] : '' ?></textarea>
                    </div>
                </div>

                <div class="form_input">
                    <label><b>Text in <span class="lang_red">Czech</span></b></label>
                    <div class="input_group">
                        <textarea id="editorsone" class="input_text input_text_home" name='text_cz' placeholder="Description"><?= isset($params['result']['text_cz']) ? $params['result']['text_cz'] : '' ?></textarea>
                    </div>
                </div>


                <hr/>

                <div class="form_input">
                    <label><b>Date</b></label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text input_text_home" name='data' placeholder="Date" value='<?= isset($params['result']['data']) ? $params['result']['data'] : '' ?>'>
                    </div>
                </div>
                <div class="form_input">
                    <div class="input_group">
                        <input type="hidden"  name='type' value='<?= isset($params['result']['type']) ? $params['result']['type'] : $params['event_type'] ?>'>
                    </div>
                </div>

                <hr/>

                <div class="form_input">
                    <div class="form_input">
                        <label>Upload Galleri image (.jpg,.png)</label>
                        <div class="input_group">
                            <div class='_foto_block foto_block forempty'>
                                <img src='<?=$baseurlM?>/assets/images/content/events/<?= isset($params['result']['image']) && $params['result']['image'] != '' ? $params['result']['image'] : ''?>' />
                                <input type="file" name="image" class="img_file" >
                                <div class='empty_foto'><i class="fa fa-picture-o"></i></div>
                                <div class='full_foto'></div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="real_img" value="<?= isset($params['result']['image']) && $params['result']['image'] != '' ? $params['result']['image'] : ''?>" >
                </div>


                <div class="clear"></div>
                <div class="form_input a_form_butt">
                    <div class="input_group clen">
                        <div class="input_img forsave"><i class="fa fa-floppy-o"></i></div>
                        <button class='save' for='main_form'>Save</button>
                    </div>
                </div>
            </div>


        </div>
    </form>
</div>

<script>
    $( function() {
        $( "#datepicker" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
    $('#editors').ckeditor();
    $('#editorsone').ckeditor();
</script>




<style>
    .a_product_img_del {
        position: absolute;
        display: none;
        top: 100px;
        font-size: 50px;
        cursor: pointer;
        z-index: 100;
    }

    .input_group {
        position: relative;
    }
    .foto_block {
        width: 250px;
        height: 150px;
        border: 1px solid;
        background: #eee;
        cursor: pointer;
        position: relative;
    }
    .input_group:hover .a_product_img_del {
        display: block;
    }
    .foto_bloch_lefts{
        float:left;
    }
</style>