<div id="menu">
<!--    <div class='user_panel'>-->
<!--        <div class="pull-left image">-->
<!--            <img src="--><?//= $baseurl ?><!--/a_assets/images/user/1.jpg" class="img-circle" alt="User Image">-->
<!--        </div>-->
<!--        <div class="pull-left info">-->
<!--            <p>Avenue</p>-->
<!--            <a href="#"><i class="fa fa-circle"></i> Online</a>-->
<!--        </div>-->
<!--    </div>-->
    <ul class='sidebar-menu'>
        <li class='header'>NAVIGATION</li>
        <a href='<?= $baseurl ?>/eventsselect/'>
            <li class='sub_menu <?= $page == 'eventsselect' ? "sub_active'" : "" ?> '>
                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                <span>Events</span>
            </li>
        </a>
        <a href='<?= $baseurl ?>/menuselect/'>
            <li class='sub_menu <?= $page == 'menuselect' ? "sub_active'" : "" ?> '>
                <i class="fa fa-cutlery"></i>
                <span>Menu</span>
            </li>
        </a>
        <a href='<?= $baseurl ?>/gallery/'>
            <li class='sub_menu <?= $page == 'gallery' ? "sub_active'" : "" ?> '>
                <i class="fa fa-picture-o" aria-hidden="true"></i>
                <span>Gallery</span>
            </li>
        </a>
        <a href='<?= $baseurl ?>/music/'>
            <li class='sub_menu <?= $page == 'music' ? "sub_active'" : "" ?> '>
                <i class="fa fa-music" aria-hidden="true"></i>
                <span>Music</span>
            </li>
        </a>
        <a href='<?= $baseurl ?>/aboutrest/'>
            <li class='sub_menu <?= $page == 'aboutrest' ? "sub_active'" : "" ?> '>
                <i class="fa fa-text-width" aria-hidden="true"></i>
                <span>About Restaurant</span>
            </li>
        </a>
        <a href='<?= $baseurl ?>/chef/'>
            <li class='sub_menu <?= $page == 'chef' ? "sub_active'" : "" ?> '>
                <i class="fa fa-text-width" aria-hidden="true"></i>
                <span>Chef</span>
            </li>
        </a>
    </ul>
</div>
<style>
    li.sub_menu {
        padding: 10px !important;
        font-size: 15px !important;
        color: #b8c7ce;
        cursor: pointer;
    }
</style>