function myFunctionsss(){
    $('.f_fid').fadeOut();
}

$(document).ready(function () {
    document.cookie = "video=off";
    var nameCook = 'video';
    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    var GlobalWidth = $(window).width();
    var GlobalHeight = $(window).height();
    var GlobalHeaderHeight = 70;
    var GlobalPageHeight = GlobalHeight - GlobalHeaderHeight;

    $('.d_main').height(GlobalPageHeight);
    $('.d_sections').height(GlobalPageHeight/4);
    function requestPost(url,body,callback){
        var oAjaxReq = new XMLHttpRequest();
        oAjaxReq.open("post", url, true);
        oAjaxReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        oAjaxReq.send(body);
        oAjaxReq.onreadystatechange = callback;
    }

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }
    $('.c_l_main').height(GlobalPageHeight);
    $('.c_r_main').height(GlobalPageHeight);
    $('.menu_items').height(GlobalPageHeight);
    // $('.r_right_big_img').height(parseInt(GlobalPageHeight) - 2);
    $('.r_right_big_img').height(GlobalPageHeight);
    $('.res_map_main').height(parseInt(GlobalPageHeight) - 2);
    $('#map').height(GlobalPageHeight);

    $('.__send_rev').click(function () {
        if(GlobalWidth <= 425){
            var name =$('.rev_name');
            var Pname = name[0].value;
            var phone =$('.rev_phone');
            var Pphone = phone[0].value;
            var email =$('.rev_email');
            var Pemail = email[0].value;
            var text =$('.rev_text');
            var Ptext = text[0].value;

        }
        if(GlobalWidth > 425){
            var name =$('.rev_name');
            var Pname = name[1].value;
            var phone =$('.rev_phone');
            var Pphone = phone[1].value;
            var email =$('.rev_email');
            var Pemail = email[1].value;
            var text =$('.rev_text');
            var Ptext = text[1].value;
        }
        if(Pname.length > 1 && Pphone.length > 1 && Pemail.length > 1 && Ptext.length > 1 ){
            var url = base+'/message/review/';
            var body = 'name='+Pname+'&phone='+Pphone+'&email='+Pemail+"&text="+Ptext;
            requestPost(url,body,function(){
                if(this.readyState == 4){
                    var result = JSON.parse(this.responseText);
                    if( result.error == false){
                        $('.rev_send_pop').css({
                            'top':'200px'
                        })
                        setTimeout(function () {
                            $('.rev_send_pop').fadeOut();
                        },4000);
                    }
                    if( result.error == true){
                        alert('Not');
                    }
                }
            })
        }else{
            alert('Please Fill All the Fields');
        }
    });

    $('.__send_wr').click(function () {
        var name =$('.wr_name').val();
        var phone =$('.wr_phone').val();
        var email =$('.wr_email').val();
        var text =$('.wr_text').val();
        if(name.length > 1 && phone.length > 1 && email.length > 1 && text.length > 1 ){
            var url = base+'/message/contact/';
            var body = 'name='+name+'&phone='+phone+'&email='+email+"&text="+text;
            requestPost(url,body,function(){
                if(this.readyState == 4){
                    var result = JSON.parse(this.responseText);
                    if( result.error == false){
                        $('.rev_send_pop').css({
                            'top':'200px'
                        })
                        setTimeout(function () {
                            $('.rev_send_pop').fadeOut();
                        },4000);
                        var name =$('.wr_name').val('');
                        var phone =$('.wr_phone').val('');
                        var email =$('.wr_email').val('');
                        var text =$('.wr_text').val('');
                    }
                    if( result.error == true){
                        alert('Not');
                    }
                }
            })
        }else{
            alert('Please Fill All the Fields');
        }
    });

    $('.res_butt').click(function () {
        var name =$('.res_name').val();
        var phone =$('.res_phone').val();
        var email =$('.res_email').val();
        var guestCount =$('.res_guest_count').val();
        var resData =$('.res_data').val();
        var resTimeStart =$('.res_time_start').val();
        var resTimeEnd =$('.res_time_end').val();
        var notes =$('.res_notes').val();
        var newslatter =$('.newslatter').prop('checked');
        var letter;
        if(newslatter == false){
            letter = 0;
        }else{
            letter = 1;
        }

        var url = base+'/message/reservation/';
        var body = 'name='+name+'&phone='+phone+'&email='+email+"&guest_count="+guestCount+'&res_data='+resData+'&res_time_start='+resTimeStart+'&res_time_end='+resTimeEnd+'&notes='+notes+'&newslatter='+letter;
        requestPost(url,body,function(){
            if(this.readyState == 4){
                var result = JSON.parse(this.responseText);
                if( result.error == false){
                    $('.rev_send_pop').css({
                        'top':'200px'
                    })
                    setTimeout(function () {
                        $('.rev_send_pop').fadeOut();
                    },4000);
                }
                if( result.error == true){
                    alert('Not');
                }
            }
        })
    });
    $( window ).resize(function() {
        var GlobalHeight = $(window).height();
        var GlobalHeaderHeight = 70;
        var GlobalPageHeight = GlobalHeight - GlobalHeaderHeight;
        $('.menu_items').height(GlobalPageHeight);
        $('.c_l_main').height(GlobalPageHeight);
        $('.c_r_main').height(GlobalPageHeight);
        $('.r_right_big_img').height(GlobalPageHeight);
        $('#map').height(GlobalPageHeight);
        if(window.innerWidth <= 768){
            if(window.innerWidth-100 > window.innerHeight){
                alert("Please use Portrait! Dont Rotate your Device!");
            }

        }
    });


    $(".rest_but").click(function(e) {
        $(".events_sub_head").slideUp(100);
        $(".gallery_sub_head").slideUp(100);
        $(".restaurant_sub_head").slideDown(200);
        e.stopPropagation();
    });
    $(".events_but").click(function(e) {
        $(".restaurant_sub_head").slideUp(100);
        $(".gallery_sub_head").slideUp(100);
        $(".events_sub_head").slideDown(200);
        e.stopPropagation();
    });
    $(".gallery_but").click(function(e) {
        $(".events_sub_head").slideUp(100);
        $(".restaurant_sub_head").slideUp(100);
        $(".gallery_sub_head").slideDown(200);
        e.stopPropagation();
    });
    $(document).click(function(e) {
        if (!$(e.target).is('.restaurant_sub_head, .restaurant_sub_head *')) {
            $(".restaurant_sub_head").slideUp(200);
        }
        if (!$(e.target).is('.events_sub_head, .events_sub_head *')) {
            $(".events_sub_head").slideUp(200);
        }
        if (!$(e.target).is('.gallery_sub_head, .gallery_sub_head *')) {
            $(".gallery_sub_head").slideUp(200);
        }
    });


    $('.res_row_place').click(function () {
        $(this).css({'display':'none'})
    })
    //     $('.c_write_main').height(0);
    //     $('.c_write_main').width(0);
    // $('.c_l_write span').click(function () {
    //     setTimeout(function(){
    //         $('.c_write_main').width($(window).width());
    //         $('.c_write_main').height(GlobalHeight);
    //     },2000)
    // })




    $('.arrow_nexts').click(function () {
        var pageIndex = $(this).attr('data-index');
        var IntIndex = parseInt(pageIndex);
        var tt = IntIndex * GlobalHeight;
        var body = $("html, body");
        body.stop().animate({scrollTop:tt}, 500);
    })


    $('.rest_menus').on('click',function () {
        var pageIndex = $(this).attr('data-ind');
        var sert =  $(this);
        var IntIndex = parseInt(pageIndex);
        var tt = IntIndex * GlobalHeight;
        var body = $("html, body");
        body.stop().animate({scrollTop:tt}, 500);
        $('.rest_menus').removeClass('restaurant_sub_head_active');
        $(this).addClass('restaurant_sub_head_active');
        $('.g_lis li a').removeClass('active');
        var headData = $('.g_lis li a[data-in='+pageIndex+']').addClass('active');
    })

    if(GlobalWidth > 425){
        $( window ).scroll(function() {
            var actPage = $('.g_lis li').find('.active');
            var pageIndex = actPage.attr('data-in');
            $('.rest_menus').removeClass('restaurant_sub_head_active');
            var headData = $('.rest_menus[data-ind='+pageIndex+']').addClass('restaurant_sub_head_active');

            var scroling = $(document).scrollTop();

            var MyWidth = $('.land_cont').width();
            var myTokos = 255*100/MyWidth;
            var opsTokos = 635*100/MyWidth;
            var opTokos = (100-myTokos)/2;
            var textTokos = (100-opsTokos)/2;
            if(scroling == GlobalHeight){
                $('.chef_img').css({
                    'margin-left':opTokos+'%'
                });
                $('.chef_title').css({
                    'text-align':'center',
                    'opacity':'1'
                });
                $('.chef_text').css({
                    'margin-left':textTokos+'%'
                });
                $('.chef_name').css({
                    'opacity':'1'
                });
                $('.chef_cit').css({
                    'opacity':'1'
                });
            }

        });
    }

    $('._langs').click(function() {
        var langs = $(this).attr('data-lang');
        document.cookie = "lang="+ langs +"; path=/";
        location.reload();
    })
    $('.mob_button, .mob_close_page').click(function () {
        $('.navigation_mob').toggleClass('menuOpen');
    })

})

    setTimeout(function(){
    $('.about_title').css({
        'margin-top':'0px'
    });
    },400);
    setTimeout(function(){
        $('.about_text').css({
            'margin-top':'0px'
        });
    },150);