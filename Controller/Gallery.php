<?php
/**
 * Created by PhpStorm.
 * User: maruq
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;
use Model\GalleryVideo;
use Model\GalleryItems;
class Gallery extends BaseController
{
    public function __construct($route = FALSE,$countRoute= FALSE)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'gallery') {
                $this->renderNotFound('main');
                die();
            }elseif($countRoute == 2 && $route[0] == 'gallery' && $route[1] == 'food' || $route[1] == 'events' || $route[1] == 'interior'){
                $this->GalleryImages($route[1]);
            }elseif($countRoute == 2 && $route[0] == 'gallery' && $route[1] == 'video'){
                $this->GalleryVideo();
            }else{
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){

        }
    }
    private function GalleryImages($page)
    {
        if($page == 'interior'){
            $id = 1;
        }
        if($page == 'food'){
            $id = 2;
        }
        if($page == 'events'){
            $id = 3;
        }
        $oGalleryItems = new GalleryItems();
        $aGalleryItems = $oGalleryItems->findByName(array('fild_name'=>'gid','fild_val'=>$id,'order'=>array('asc'=>'ord')));
        $this->result['result'] = $aGalleryItems;
        $this->renderView("Pages/gallery/imagegallery",'imagegallery',$this->result);
    }
    private function GalleryVideo()
    {
        $oGalleryVideo = new GalleryVideo;
        $aGalleryVideo = $oGalleryVideo->findAll(array('order'=>array('asc'=>'ord')));
        $this->result['result'] = $aGalleryVideo;
        $this->renderView("Pages/gallery/videogallery",'videogallery',$this->result);
    }
}