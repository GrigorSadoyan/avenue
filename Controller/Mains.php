<?php
namespace Controller;
use Core\Controller as BaseController;
use Model\Music as MusicM;
use Model\ChefText;
use Model\AboutRestaurant;

class Mains extends BaseController{

    public function __construct()
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET'){
            $this->index();
        }else{
            $this->renderNotFound('main');
            die();
        }
    }

    public function index()
    {
        $oChefText = new ChefText();
        $this->result['chef_text'] = $oChefText->findById('1');
        $oAboutRestaurant = new AboutRestaurant();
        $this->result['about_rest'] = $oAboutRestaurant->findById('1');
        $_oMusic = new MusicM();
        $_aMusic = $_oMusic->findRand();
        $this->result['music'] = $_aMusic;
        $this->renderView("Pages/main","main",$this->result);
    }
}
