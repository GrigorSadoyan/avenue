<?php
/**
 * Created by PhpStorm.
 * User: maruq
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;
use Model\Events as EventsModel;
use Model\EventGallery;
class Events extends BaseController
{
    public function __construct($route = FALSE,$countRoute= FALSE)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'events') {
                $this->renderNotFound('main');
                die();
            }elseif($countRoute == 2 && $route[0] == 'events' && is_string($route[1])){
                $this->EventPage($route[1]);
            }elseif($countRoute == 3 && $route[0] == 'events' && is_string($route[1]) && is_numeric($route[2])){
                $this->ThisEventArchive($route[1],$route[2]);
            }else{
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){

        }
    }
    private function EventPage($page)
    {
        if($page == 'upcoming'){
            $id = '1';
        }elseif($page == 'archive'){
            $id = '2';
        }
        $_oEventsModel = new EventsModel();
        $aEvents = $_oEventsModel->findByName(array('fild_name'=>'type','fild_val'=>$id,'order'=>array('asc'=>'ord')));
        $this->result['result'] = $aEvents;
        $this->result['pages'] = $page;
        $this->renderView("Pages/events/event",'event',$this->result);
    }
    private function ThisEventArchive($page,$id){
        $_oEventsModel = new EventsModel();
        $aEvents = $_oEventsModel->findById($id);


        $_oEventGallery = new EventGallery();
        $aEvents['gall'] = $_oEventGallery->findByName(array('fild_name'=>'eid','fild_val'=>$aEvents['id'],'order'=>array('asc'=>'ord')));
        $this->result['result'] = $aEvents;
//        echo '<pre>';
//        var_dump($aEvents);die;
        $this->renderView("Pages/events/thisevent",'thisevent',$this->result);
    }
}