<?php
/**
 * Created by PhpStorm.
 * User: maruq
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;

class Contacts extends BaseController
{
    public function __construct($route = FALSE,$countRoute= FALSE)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'contacts') {
                $this->index();
            }else{
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){

        }
    }
    private function index()
    {
        $this->renderView("Pages/contacts/contacts",'contacts',$this->result);
    }
}