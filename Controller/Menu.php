<?php
/**
 * Created by PhpStorm.
 * User: maruq
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;
use Model\Menu as MenuM;
use Model\Menucats;
use Model\MenuItems;

class Menu extends BaseController
{
    public function __construct($route = FALSE,$countRoute= FALSE)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'menu') {
                $this->index();
            }elseif($countRoute == 3 && $route[0] == 'menu' && is_string($route[1]) && is_numeric($route[2])){
                $this->MenuItem($route[1],$route[2]);
            }else{
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){

        }
    }
    private function index()
    {
        $this->renderView("Pages/menu/menu",'menu',$this->result);
    }
    private function MenuItem($menuName, $menuId){
        $_oMenuM = new MenuM();
        $_aMenuM = $_oMenuM->findAll(array());
        $this->result['menus'] = $_aMenuM;
        $this->result['menu_type'] = $menuName;

        $_oMenucats = new Menucats();
        $aMenucats = $_oMenucats->findByName(array('fild_name'=>'mid','fild_val'=>$menuId,'order'=>array('asc'=>'ord')));
        $this->result['menu_cats'] = $aMenucats;

        $oItems = new MenuItems();
        for ($i = 0; $i < count($aMenucats); $i++){
            $aMenucats[$i]['sub_menu'] = $oItems->findByName(array('fild_name'=>'mcid','fild_val'=>$aMenucats[$i]['id'],'order'=>array('asc'=>'ord')));
        }
        $this->result['menu_all'] = $aMenucats;
        $this->renderView("Pages/menu/menuitem",'menuitem',$this->result);
    }
}