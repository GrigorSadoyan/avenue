<?php
/**
 * Created by PhpStorm.
 * User: maruq
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;
use Model\Review;
use Model\Reservation;
use Model\Contact;
use Model\Subscribe;

class Message extends BaseController
{
    public function __construct($route = FALSE,$countRoute= FALSE)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->renderNotFound('main');
            die();
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if ($countRoute == 2 && $route[0] == 'message' && $route[1] == 'review') {
                $this->SendReview();
            }else if ($countRoute == 2 && $route[0] == 'message' && $route[1] == 'contact') {
                $this->SendContact();
            }else if ($countRoute == 2 && $route[0] == 'message' && $route[1] == 'reservation') {
                $this->SendReserve();
            }else{
                $this->renderNotFound('main');
                die();
            }
        }
    }
    private function SendReview()
    {
        $_oReview = new Review();
        $_oReview->_post=$_POST;
        $_oReview->insert();
        $message = " New Review in AvenueRestobar \r\n\r\n  Name - ".$_POST['name'].", \r\n Phone - ".$_POST['phone'].", \r\n Email - ".$_POST['email'].", \r\n Text - ".$_POST['text'];
        $ok = mail('info@avenuerestaurant.cz', 'New Review in AvenueRestobar', $message);
        if($ok){
            echo json_encode(array(
                'error'=>false,
                'msg' => 'Msg send Correctly'
            ));
        }else{
            echo json_encode(array(
                'error'=>true,
                'msg' => 'Msg Not send'
            ));
        }

    }
    private function SendContact()
    {
        $_oContact = new Contact();
        $_oContact->_post=$_POST;
        $_oContact->insert();
        $message = " New Contact Message in AvenueRestobar \r\n\r\n  Name - ".$_POST['name'].", \r\n Phone - ".$_POST['phone'].", \r\n Email - ".$_POST['email'].", \r\n Text - ".$_POST['text'];
        $ok = mail('info@avenuerestaurant.cz', 'Contact Message in AvenueRestobar', $message);
        if($ok){
            echo json_encode(array(
                'error'=>false,
                'msg' => 'Msg send Correctly'
            ));
        }else{
            echo json_encode(array(
                'error'=>true,
                'msg' => 'Msg Not send'
            ));
        }

    }
    private function SendReserve(){
        $subscribe = $_POST['newslatter'];
        unset($_POST['newslatter']);
        $_oReservation = new Reservation();
        $_oReservation->_post=$_POST;
        $lastId = $_oReservation->insert();

        if($subscribe == '1'){
            $_oSubscribe = new Subscribe();
            $_newpost['email'] = $_POST['email'];
            $_oSubscribe->_post=$_newpost;
            $_oSubscribe->insert();
        }

        if($lastId){
            echo json_encode(array(
                'error'=>false,
                'msg' => 'Msg send Correctly'
            ));
        }else{
            echo json_encode(array(
                'error'=>true,
                'msg' => 'Msg Not send'
            ));
        }
    }
}