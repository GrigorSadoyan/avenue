<?php
/**
 * Created by PhpStorm.
 * User: sadoy
 * Date: 3/17/2018
 * Time: 1:53 PM
 */

namespace Model;
use Core\Model;
use \PDO;
class Music extends Model
{

    protected $table = 'music';
    protected  $pdoObject;
    public $result;

    public function __construct()
    {
        parent::__construct();
    }

    public function findRand(){
        $this->pdoObject = $this->db->prepare("SELECT * FROM `$this->table` ORDER BY RAND() LIMIT 1");
        $this->pdoObject->execute();
        return $this->pdoObject->fetchAll(PDO::FETCH_ASSOC);
    }

}